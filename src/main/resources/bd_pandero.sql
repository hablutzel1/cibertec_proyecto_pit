-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0;
SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0;
SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bd_pandero
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema bd_pandero
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bd_pandero`
  DEFAULT CHARACTER SET utf8;
USE `bd_pandero`;

-- -----------------------------------------------------
-- Table `bd_pandero`.`socio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_pandero`.`socio` (
  `codigo`                       INT(11)      NOT NULL AUTO_INCREMENT,
  `dni`                          VARCHAR(45)  NULL     DEFAULT NULL,
  `nombres`                      VARCHAR(255) NULL     DEFAULT NULL,
  `apellidos`                    VARCHAR(255) NULL     DEFAULT NULL,
  `direccion`                    VARCHAR(255) NULL     DEFAULT NULL,
  `datos_del_conyuge`            VARCHAR(255) NULL     DEFAULT NULL,
  `hijos`                        INT(11)      NULL     DEFAULT NULL,
  `imagen`                       MEDIUMBLOB   NULL     DEFAULT NULL,
  `huella`                       MEDIUMBLOB   NULL     DEFAULT NULL,
  `firma`                        MEDIUMBLOB   NULL     DEFAULT NULL,
  `ultima_razon_de_modificacion` VARCHAR(255) NULL     DEFAULT NULL,
  `fecha_nacimiento`             DATE         NULL,
  `telefono`                     VARCHAR(45)  NULL,
  PRIMARY KEY (`codigo`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `bd_pandero`.`obligacion_de_pago`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_pandero`.`obligacion_de_pago` (
  `codigo`                 INT(11)        NOT NULL AUTO_INCREMENT,
  `descripcion`            VARCHAR(255)   NULL     DEFAULT NULL,
  `monto`            DECIMAL(10, 2) NULL     DEFAULT NULL,
  `num_cuotas`             INT(11)        NULL     DEFAULT NULL,
  `fecha_emision`          DATE           NULL     DEFAULT NULL,
  `documentos_de_sustento` MEDIUMBLOB     NULL     DEFAULT NULL,
  `socio_codigo`           INT(11)        NOT NULL,
  PRIMARY KEY (`codigo`),
  INDEX `fk_obligacion_de_pago_socio_idx` (`socio_codigo` ASC),
  CONSTRAINT `fk_obligacion_de_pago_socio`
  FOREIGN KEY (`socio_codigo`)
  REFERENCES `bd_pandero`.`socio` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 14
  DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `bd_pandero`.`cuota`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_pandero`.`cuota` (
  `obligacion_de_pago_codigo` INT(11)        NOT NULL,
  `codigo`                    INT(11)        NOT NULL,
  `monto`                     DECIMAL(10, 2) NULL DEFAULT NULL,
  `fecha_vencimiento`         DATE           NULL DEFAULT NULL,
  `fecha_pago`                DATE           NULL DEFAULT NULL,
  `monto_pagado`              DECIMAL(10, 2) NULL DEFAULT NULL,
  `comprobante_de_pago`       VARCHAR(45)    NULL DEFAULT NULL,
  PRIMARY KEY (`obligacion_de_pago_codigo`, `codigo`),
  CONSTRAINT `fk_cuota_obligacion_de_pago1`
  FOREIGN KEY (`obligacion_de_pago_codigo`)
  REFERENCES `bd_pandero`.`obligacion_de_pago` (`codigo`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- Table `bd_pandero`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `bd_pandero`.`usuario` (
  `nombre_usuario` VARCHAR(45) NOT NULL,
  `contrasena`     VARCHAR(45) NULL DEFAULT NULL,
  `perfil`         VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`nombre_usuario`)
)
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8;


SET SQL_MODE = @OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS;

INSERT INTO `bd_pandero`.`usuario` (`nombre_usuario`, `contrasena`, `perfil`)
VALUES ('admin', 'admin', 'administrador');
