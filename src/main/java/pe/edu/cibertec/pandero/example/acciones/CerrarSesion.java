package pe.edu.cibertec.pandero.example.acciones;

public class CerrarSesion extends AccionBase {

    @Override
    public String execute() throws Exception {
        sesion.remove("usuario");
        addActionMessage("Se cerró sesión correctamente.");
        return SUCCESS;
    }

}
