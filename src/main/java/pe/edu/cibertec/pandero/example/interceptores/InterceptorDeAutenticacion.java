package pe.edu.cibertec.pandero.example.interceptores;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;

public class InterceptorDeAutenticacion extends AbstractInterceptor{
    @Override
    public String intercept(ActionInvocation invocation) throws Exception {
        final Object login = invocation.getInvocationContext().getSession().get("usuario");
        if (login != null) {
            return invocation.invoke();
        } else {
            return Action.LOGIN;
        }
    }
}
