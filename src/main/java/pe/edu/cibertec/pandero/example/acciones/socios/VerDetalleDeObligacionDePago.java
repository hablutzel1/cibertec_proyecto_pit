package pe.edu.cibertec.pandero.example.acciones.socios;

import pe.edu.cibertec.pandero.example.acciones.AccionBase;
import pe.edu.cibertec.pandero.example.modelo.ObligacionDePago;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public class VerDetalleDeObligacionDePago extends AccionBase {

    public InputStream getFlujoDeDocumento() {
        return flujoDeDocumento;
    }

    public void setFlujoDeDocumento(InputStream flujoDeDocumento) {
        this.flujoDeDocumento = flujoDeDocumento;
    }

    public String getNombreDeDocumento() {
        return nombreDeDocumento;
    }

    public void setNombreDeDocumento(String nombreDeDocumento) {
        this.nombreDeDocumento = nombreDeDocumento;
    }

    private InputStream flujoDeDocumento;

    private String nombreDeDocumento;

    private ObligacionDePago obligacionDePago;

    public ObligacionDePago getObligacionDePago() {
        return obligacionDePago;
    }

    public void setObligacionDePago(ObligacionDePago obligacionDePago) {
        this.obligacionDePago = obligacionDePago;
    }

    @Override
    public String execute() throws Exception {
        return super.execute();
    }

    public String documento() throws Exception {
        for (Map.Entry<String, byte[]> entradasDeDocumentosDeSustento : obligacionDePago.getDocumentosDeSustento().entrySet()) {
            if (entradasDeDocumentosDeSustento.getKey().equals(nombreDeDocumento)) {
                flujoDeDocumento = new ByteArrayInputStream(entradasDeDocumentosDeSustento.getValue());
                break;
            }
        }
        return SUCCESS;
    }

    @Override
    public void validate() {
        try {
            obligacionDePago.recargar();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
