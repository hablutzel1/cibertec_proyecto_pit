package pe.edu.cibertec.pandero.example.acciones.reportes;

import org.apache.commons.io.input.ReaderInputStream;
import pe.edu.cibertec.pandero.example.acciones.AccionBase;
import pe.edu.cibertec.pandero.example.modelo.Cuota;

import java.io.InputStream;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AccionBaseDeReportes extends AccionBase {

    static final String NOHAYDATOS = "nohaydatos";
    protected List<HashMap<String, Object>> fuenteDeDatos;
    private InputStream flujoNoHayDatos;

    public AccionBaseDeReportes() {
        flujoNoHayDatos = new ReaderInputStream(new StringReader("No hay datos."));
    }

    static List<HashMap<String, Object>> transformarAFuenteDeDatos(List<Cuota> cuotas) {
        List<HashMap<String, Object>> entradasDeReporte = new ArrayList<>();
        for (Cuota cuota : cuotas) {
            HashMap<String, Object> entrada = new HashMap<>();
            entrada.put("cuota_codigo", cuota.getCodigo());
            entrada.put("cuota_monto", BigDecimal.valueOf(cuota.getMonto()));
            entrada.put("cuota_fecha_vencimiento", new java.sql.Date(cuota.getFechaVencimiento().getTime()));
            if (cuota.getFechaPago() != null) {
                entrada.put("cuota_fecha_pago", new java.sql.Date(cuota.getFechaPago().getTime()));
                entrada.put("cuota_monto_pagado", BigDecimal.valueOf(cuota.getMontoPagado()));
                entrada.put("cuota_comprobante_de_pago", cuota.getComprobanteDePago());
            }
            entrada.put("obligacion_de_pago_codigo", cuota.getObligacionDePago().getCodigo());
            entrada.put("obligacion_de_pago_descripcion", cuota.getObligacionDePago().getDescripcion());
            entrada.put("obligacion_de_pago_monto", BigDecimal.valueOf(cuota.getObligacionDePago().getMonto()));
            entrada.put("obligacion_de_pago_num_cuotas", cuota.getObligacionDePago().getNumeroDeCuotas());
            entrada.put("socio_codigo", cuota.getObligacionDePago().getSocio().getCodigo());
            entrada.put("socio_dni", cuota.getObligacionDePago().getSocio().getDni());
            entrada.put("socio_nombres", cuota.getObligacionDePago().getSocio().getNombres());
            entrada.put("socio_apellidos", cuota.getObligacionDePago().getSocio().getApellidos());
            entradasDeReporte.add(entrada);
        }
        return entradasDeReporte;
    }

    public InputStream getFlujoNoHayDatos() {
        return flujoNoHayDatos;
    }

    public void setFlujoNoHayDatos(InputStream flujoNoHayDatos) {
        this.flujoNoHayDatos = flujoNoHayDatos;
    }

    public List<HashMap<String, Object>> getFuenteDeDatos() {
        return fuenteDeDatos;
    }

    public void setFuenteDeDatos(List<HashMap<String, Object>> fuenteDeDatos) {
        this.fuenteDeDatos = fuenteDeDatos;
    }
}
