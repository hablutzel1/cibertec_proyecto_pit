package pe.edu.cibertec.pandero.example.modelo;

import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

public class Cuota extends UnidadDePersistencia {

    /**
     * Esto significa <code>1%</code> de interés diario sobre el valor de la cuota.
     */
    public static final int PORCENTAJE_DE_INTERES_DIARIO = 1;

    private ObligacionDePago obligacionDePago;
    private int codigo;
    private double monto;
    private Date fechaVencimiento;
    private String comprobanteDePago;
    private Date fechaPago;
    private double montoPagado;

    public static List<Cuota> obtenerCuotasPagadas(Date fecha) {
        try (SqlSession sqlSession = getSqlSession()) {
            return sqlSession.selectList("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.buscarCuotasPagadas", fecha);
        }
    }

    public static List<Cuota> obtenerCuotasSinPagar(Date fecha) {
        try (SqlSession sqlSession = getSqlSession()) {
            return sqlSession.selectList("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.buscarCuotasSinPagar", fecha);
        }
    }

    public static List<Cuota> obtenerCuotasVencidas() {
        try (SqlSession sqlSession = getSqlSession()) {
            Date hoyAlInicioDelDia = new DateTime().withTimeAtStartOfDay().toDate();
            return sqlSession.selectList("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.buscarCuotasVencidas", hoyAlInicioDelDia);
        }
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public ObligacionDePago getObligacionDePago() {
        return obligacionDePago;
    }

    public void setObligacionDePago(ObligacionDePago obligacionDePago) {
        this.obligacionDePago = obligacionDePago;
    }

    public String getComprobanteDePago() {
        return comprobanteDePago;
    }

    public void setComprobanteDePago(String comprobanteDePago) {
        this.comprobanteDePago = comprobanteDePago;
    }

    /**
     * @return -1 si no hay intereses a la fecha o si los intereses no aplican para esta cuota
     */
    public double getInteresesALaFecha() {
        DateTime hoyAlInicioDelDía = new DateTime().withTimeAtStartOfDay();
        DateTime fechaVencimiento = new DateTime(this.fechaVencimiento.getTime());
        if (fechaPago == null && fechaVencimiento.isBefore(hoyAlInicioDelDía)) {
            int diasTranscurridos = Days.daysBetween(fechaVencimiento, hoyAlInicioDelDía).getDays();
            // FIXME todas estas operaciones con números de punto flotante podrían estar generando pérdida de precisión (ej. durante el redondeo).
            double interes = monto * (PORCENTAJE_DE_INTERES_DIARIO / 100.0) * diasTranscurridos;
            BigDecimal decimal = new BigDecimal(interes);
            decimal = decimal.setScale(2, RoundingMode.HALF_UP);
            return decimal.doubleValue();
        } else {
            return -1;
        }
    }

    void recargar() {
        try (SqlSession sqlSession = getSqlSession()) {
            Cuota obligacionDePagoDeBD = sqlSession.selectOne("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.obtener", this);
            this.obligacionDePago = obligacionDePagoDeBD.obligacionDePago;
            this.monto = obligacionDePagoDeBD.monto;
            this.fechaVencimiento = obligacionDePagoDeBD.fechaVencimiento;
            this.comprobanteDePago = obligacionDePagoDeBD.comprobanteDePago;
            this.fechaPago = obligacionDePagoDeBD.fechaPago;
            this.montoPagado = obligacionDePagoDeBD.montoPagado;
        }
    }

    void setMontoPagado(double montoPagado) {
        this.montoPagado = montoPagado;
    }

    public double getMontoPagado() {
        return montoPagado;
    }
}
