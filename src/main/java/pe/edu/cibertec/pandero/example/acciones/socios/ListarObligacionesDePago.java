package pe.edu.cibertec.pandero.example.acciones.socios;

import com.opensymphony.xwork2.ActionSupport;
import pe.edu.cibertec.pandero.example.modelo.Socio;

public class ListarObligacionesDePago extends ActionSupport {

    private Socio socio;

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    @Override
    public String execute() throws Exception {
        socio = Socio.buscarPorCodigo(socio.getCodigo());
        return SUCCESS;
    }

}
