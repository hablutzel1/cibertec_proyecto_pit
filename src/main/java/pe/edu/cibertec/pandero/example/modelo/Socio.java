package pe.edu.cibertec.pandero.example.modelo;

import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Socio extends UnidadDePersistencia {

    private int codigo;
    private String nombres;
    private String apellidos;
    private Date fechaNacimiento;
    private String telefono;
    private String dni;
    private String direccion;
    private Integer hijos;
    private String datosDelConyuge;
    // TODO determinar si hay alguna manera de marcar este campo como "transient".
    private String imagenComoB64;
    private byte[] imagen;
    private byte[] huella;
    private String firmaComoB64;
    private String ultimaRazonDeModificacion;
    private byte[] firma;


    public static Socio buscarPorDNI(String dni) {
        try (SqlSession sqlSession = getSqlSession()) {
            return sqlSession.selectOne("pe.edu.cibertec.pandero.example.modelo.SocioMapper.buscarPorDNI", dni);
        }
    }

    public static Socio buscarPorCodigo(int codigo) {
        try (SqlSession sqlSession = getSqlSession()) {
            return sqlSession.selectOne("pe.edu.cibertec.pandero.example.modelo.SocioMapper.buscarPorCodigo", codigo);
        }
    }

    public static List<Socio> todos() {
        try (SqlSession sqlSession = getSqlSession()) {
            return sqlSession.selectList("pe.edu.cibertec.pandero.example.modelo.SocioMapper.todos");
        }
    }

    public List<Cuota> obtenerCuotasPagadas(Date fechaDeInicio, Date fechaDeFin) {
        try (SqlSession sqlSession = getSqlSession()) {
            HashMap<String, Object> parameter = new HashMap<>();
            parameter.put("codigoSocio", getCodigo());
            parameter.put("fechaDeInicio", fechaDeInicio);
            parameter.put("fechaDeFin", fechaDeFin);
            return sqlSession.selectList("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.buscarCuotasPagadasPorSocio", parameter);
        }
    }

    public void registrarNuevo() throws IOException {
        // TODO evaluar validar que el formato de la siguientes imagenes sea PNG siempre, nótese que la acción "VerDetalle_*" de Struts 2 está indicando PNG siempre como el 'contentType'.
        imagen = Base64.getDecoder().decode(imagenComoB64);
        firma = Base64.getDecoder().decode(firmaComoB64);
        try (SqlSession sqlSession = getSqlSession()) {
            sqlSession.insert("pe.edu.cibertec.pandero.example.modelo.SocioMapper.insertar", this);
            sqlSession.commit();
        }
    }

    public Integer getHijos() {
        return hijos;
    }

    public void setHijos(Integer hijos) {
        this.hijos = hijos;
    }

    public String getDatosDelConyuge() {
        return datosDelConyuge;
    }

    public void setDatosDelConyuge(String datosDelConyuge) {
        this.datosDelConyuge = datosDelConyuge;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getImagenComoB64() {
        return imagenComoB64;
    }

    public void setImagenComoB64(String imagenComoB64) {
        this.imagenComoB64 = imagenComoB64;
    }

    public byte[] getHuella() {
        return huella;
    }

    public void setHuella(byte[] huella) {
        this.huella = huella;
    }

    public String getFirmaComoB64() {
        return firmaComoB64;
    }

    public void setFirmaComoB64(String firmaComoB64) {
        this.firmaComoB64 = firmaComoB64;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public byte[] getFirma() {
        return firma;
    }

    public void setFirma(byte[] firma) {
        this.firma = firma;
    }

    public String getUltimaRazonDeModificacion() {
        return ultimaRazonDeModificacion;
    }

    public void setUltimaRazonDeModificacion(String ultimaRazonDeModificacion) {
        this.ultimaRazonDeModificacion = ultimaRazonDeModificacion;
    }

    public void registrarModificacion() {
        try (SqlSession sqlSession = getSqlSession()) {
            sqlSession.update("pe.edu.cibertec.pandero.example.modelo.SocioMapper.modificar", this);
            sqlSession.commit();
        }
    }

    public List<ObligacionDePago> getObligacionesDePago() {
        try (SqlSession sqlSession = getSqlSession()) {
            return sqlSession.selectList("pe.edu.cibertec.pandero.example.modelo.ObligacionDePagoMapper.buscarPorSocio", codigo);
        }
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
