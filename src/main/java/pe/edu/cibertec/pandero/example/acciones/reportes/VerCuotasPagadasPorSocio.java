package pe.edu.cibertec.pandero.example.acciones.reportes;

import pe.edu.cibertec.pandero.example.modelo.Cuota;
import pe.edu.cibertec.pandero.example.modelo.Socio;

import java.util.Date;
import java.util.List;

public class VerCuotasPagadasPorSocio extends AccionBaseDeReportes {

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    private Socio socio;
    private Date fechaDeFin;
    private Date fechaDeInicio;
    private List<Socio> todosLosSocios;


    public Date getFechaDeFin() {
        return fechaDeFin;
    }

    public void setFechaDeFin(Date fechaDeFin) {
        this.fechaDeFin = fechaDeFin;
    }

    public Date getFechaDeInicio() {
        return fechaDeInicio;
    }

    public void setFechaDeInicio(Date fechaDeInicio) {
        this.fechaDeInicio = fechaDeInicio;
    }

    public List<Socio> getTodosLosSocios() {
        return todosLosSocios;
    }

    public void setTodosLosSocios(List<Socio> todosLosSocios) {
        this.todosLosSocios = todosLosSocios;
    }

    @Override
    public String execute() throws Exception {
        return super.execute();
    }

    @Override
    public void validate() {
        todosLosSocios = Socio.todos();
        if (esPost()) {
            if (socio == null) {
                addFieldError("codigoDeSocio", "Se requiere seleccionar un socio.");
            }
            if (fechaDeInicio == null) {
                addFieldError("fechaDeInicio", "La fecha de inicio es requerida.");
            }
            if (fechaDeFin == null) {
                addFieldError("fechaDeFin", "La fecha de fin es requerida.");
            }
            // TODO validar que la fecha final no sea anterior a la fecha de inicio.
        }
    }

    public String reporte() throws Exception {
        List<Cuota> cuotas = socio.obtenerCuotasPagadas(fechaDeInicio, fechaDeFin);
        if (cuotas.isEmpty()) {
            return NOHAYDATOS;
        }
        fuenteDeDatos = AccionBaseDeReportes.transformarAFuenteDeDatos(cuotas);
        return "reporte";
    }


}