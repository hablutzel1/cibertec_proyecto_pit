package pe.edu.cibertec.pandero.example.acciones.socios;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Writer;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.krysalis.barcode4j.impl.code39.Code39Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import pe.edu.cibertec.pandero.example.modelo.Socio;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class ImprimirCarnet extends ActionSupport {

    private InputStream flujoDeImagen;

    private Socio socio;

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    @Override
    public String execute() throws Exception {
        popularSocio();
        return SUCCESS;
    }

    private void popularSocio() {
        socio = Socio.buscarPorCodigo(socio.getCodigo());
    }

    public String code39() throws Exception {
        popularSocio();
        // See http://barcode4j.sourceforge.net/2.1/embedding-bean.html.
        Code39Bean code39Bean = new Code39Bean();
        final int dpi = 150;
        code39Bean.setModuleWidth(UnitConv.in2mm(1.0f / dpi));
        code39Bean.setWideFactor(3);
        code39Bean.setHeight(11);
        code39Bean.doQuietZone(false);
        ByteArrayOutputStream pngBAOS = new ByteArrayOutputStream();
        try {
            BitmapCanvasProvider canvas = new BitmapCanvasProvider(
                    pngBAOS, "image/x-png", dpi, BufferedImage.TYPE_BYTE_BINARY, false, 0);
            code39Bean.generateBarcode(canvas, rellenarCodigoConCerosALaIzquierda());
            canvas.finish();
        } finally {
            pngBAOS.close();
        }
        this.flujoDeImagen = new ByteArrayInputStream(pngBAOS.toByteArray());
        return SUCCESS;
    }

    private String rellenarCodigoConCerosALaIzquierda() {
        return StringUtils.leftPad(String.valueOf(socio.getCodigo()), 6, '0');
    }

    public String codigoqr() throws Exception {
        popularSocio();
        int dimension = 256;
        Writer writer = new QRCodeWriter();
        BitMatrix byteMatrix = writer.encode(rellenarCodigoConCerosALaIzquierda(), BarcodeFormat.QR_CODE, dimension,
                dimension, null);
        BufferedImage bufferedImage = new BufferedImage(dimension, dimension,
                BufferedImage.TYPE_INT_RGB);
        bufferedImage.createGraphics();
        Graphics2D graphics = (Graphics2D) bufferedImage.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, dimension, dimension);
        graphics.setColor(Color.BLACK);
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (byteMatrix.get(i, j)) {
                    graphics.fillRect(i, j, 1, 1);
                }
            }
        }
        ByteArrayOutputStream pngBAOS = new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", pngBAOS);
        this.flujoDeImagen = new ByteArrayInputStream(pngBAOS.toByteArray());
        return SUCCESS;
    }

    public InputStream getFlujoDeImagen() {
        return flujoDeImagen;
    }

    public void setFlujoDeImagen(InputStream flujoDeImagen) {
        this.flujoDeImagen = flujoDeImagen;
    }
}
