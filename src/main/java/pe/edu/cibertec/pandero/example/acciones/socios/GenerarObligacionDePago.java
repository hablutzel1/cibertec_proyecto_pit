package pe.edu.cibertec.pandero.example.acciones.socios;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import pe.edu.cibertec.pandero.example.acciones.AccionBase;
import pe.edu.cibertec.pandero.example.modelo.ObligacionDePago;
import pe.edu.cibertec.pandero.example.modelo.Socio;

import java.io.File;

public class GenerarObligacionDePago extends AccionBase {

    private File[] documentosDeSustento;
    private String[] documentosDeSustentoFileNames;
    private String[] documentosDeSustentoContentTypes;

    public File[] getDocumentoDeSustento() {
        return this.documentosDeSustento;
    }

    public void setDocumentoDeSustento(File[] documentosDeSustento) {
        this.documentosDeSustento = documentosDeSustento;
    }

    public String[] getDocumentoDeSustentoFileName() {
        return this.documentosDeSustentoFileNames;
    }

    public void setDocumentoDeSustentoFileName(String[] documentosDeSustentoFileNames) {
        this.documentosDeSustentoFileNames = documentosDeSustentoFileNames;
    }

    public String[] getDocumentoDeSustentoContentType() {
        return documentosDeSustentoContentTypes;
    }

    public void setDocumentoDeSustentoContentType(String[] documentosDeSustentoContentTypes) {
        this.documentosDeSustentoContentTypes = documentosDeSustentoContentTypes;
    }

    private Socio socio;
    private ObligacionDePago obligacionDePago;

    public ObligacionDePago getObligacionDePago() {
        return obligacionDePago;
    }

    public void setObligacionDePago(ObligacionDePago obligacionDePago) {
        this.obligacionDePago = obligacionDePago;
    }

    @Override
    public String execute() throws Exception {
        verificarPerfil("secretaria");
        if (esGet()) {
            return INPUT;
        } else {
            if (documentosDeSustento != null) {
                for (int i = 0; i < documentosDeSustento.length; i++) {
                    obligacionDePago.añadirDocumentoDeSustento(documentosDeSustentoFileNames[i], documentosDeSustento[i]);
                }
            }
            obligacionDePago.generar();
            return SUCCESS;
        }
    }

    @Override
    public void validate() {
        this.socio = Socio.buscarPorCodigo(socio.getCodigo());
        if (esPost()) {
            if (StringUtils.isBlank(obligacionDePago.getDescripcion())) {
                addFieldError("obligacionDePago.descripcion", "El motivo de pago/deuda es requerido.");
            }

            if (obligacionDePago.getMonto() <= 0) {
                addFieldError("obligacionDePago.monto", "El monto total/cuota debe ser mayor que cero.");
            }

            if (documentosDeSustentoFileNames != null) {
                for (String nombreDeArchivoDeDocumentoDeSustento : documentosDeSustentoFileNames) {
                    String extension = FilenameUtils.getExtension(nombreDeArchivoDeDocumentoDeSustento).toLowerCase();
                    if (!extension.equals("pdf")) {
                        addFieldError("documentosDeSustento", "Solo se admiten documentos con formato PDF.");
                    }
                }
            }

            // TODO validar que el número de cuotas sea válido, ¿esto es 12, 24 o 36? o que de lo contrario las cuotas repetitivas estén activadas.
        }
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }
}
