package pe.edu.cibertec.pandero.example.acciones.usuarios;

import pe.edu.cibertec.pandero.example.acciones.AccionBase;
import pe.edu.cibertec.pandero.example.modelo.Usuario;

import java.util.List;

public class ListarUsuarios extends AccionBase {

    private List<Usuario> todosLosUsuarios;

    @Override
    public String execute() throws Exception {
        verificarPerfil("administrador");
        todosLosUsuarios = Usuario.todos();
        return super.execute();
    }

    public List<Usuario> getTodosLosUsuarios() {
        return todosLosUsuarios;
    }

    public void setTodosLosUsuarios(List<Usuario> todosLosUsuarios) {
        this.todosLosUsuarios = todosLosUsuarios;
    }
}
