package pe.edu.cibertec.pandero.example.acciones.reportes;

import pe.edu.cibertec.pandero.example.modelo.Cuota;

import java.util.List;

public class VerCuotasVencidas extends AccionBaseDeReportes {

    @Override
    public String execute() throws Exception {
        return super.execute();
    }

    public String reporte() throws Exception {
        List<Cuota> cuotas = Cuota.obtenerCuotasVencidas();
        if (cuotas.isEmpty()) {
            return NOHAYDATOS;
        }
        fuenteDeDatos = AccionBaseDeReportes.transformarAFuenteDeDatos(cuotas);
        return "reporte";

    }


}