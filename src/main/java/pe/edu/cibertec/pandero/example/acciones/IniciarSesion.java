package pe.edu.cibertec.pandero.example.acciones;

import pe.edu.cibertec.pandero.example.modelo.Usuario;

public class IniciarSesion extends AccionBase {

    private Usuario usuario;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String execute() throws Exception {
        if (usuario == null) {
            return INPUT;
        } else {
            Usuario usuarioEnBD;
            if ((usuarioEnBD = Usuario.buscar(usuario.getNombreUsuario())) != null && usuario.getContrasena().equals(usuarioEnBD.getContrasena())) {
                sesion.put("usuario", usuarioEnBD);
                return SUCCESS;
            } else {
                addActionError("Las credenciales provistas son incorrectas.");
                return INPUT;
            }
        }
    }

}
