package pe.edu.cibertec.pandero.example.listener;


import pe.edu.cibertec.pandero.example.modelo.ObligacionDePago;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class EscuchaDeInicioDeAplicacion implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        // TODO evaluar llamarlo todos los días a una hora fija, ej. 2am, en vez de llamarlo inmediatamente a partir del momento en el que inicia la aplicación. Revisar java.util.Timer.scheduleAtFixedRate(java.util.TimerTask, java.util.Date, long).
        // TODO confirmar que esto se esté ejecutando cada día como se espera, derepente escribiendo a un archivo de registro, ej. el del Tomcat.
        scheduledExecutorService.scheduleAtFixedRate(ObligacionDePago::generarCuotasRepetitivasDeSerNecesario, 0, 1, TimeUnit.DAYS);
    }

    public void contextDestroyed(ServletContextEvent sce) {
    }

}
