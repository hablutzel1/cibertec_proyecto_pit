package pe.edu.cibertec.pandero.example.acciones.socios;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import pe.edu.cibertec.pandero.example.acciones.AccionBase;
import pe.edu.cibertec.pandero.example.modelo.ObligacionDePago;

import java.io.File;
import java.io.IOException;

public class ModificarObligacionDePago extends AccionBase {

    private File[] documentosDeSustentoParaModificacion;
    private String[] documentosDeSustentoParaModificacionFileNames;
    private String[] documentosDeSustentoParaModificacionContentTypes;

    public File[] getDocumentoDeSustentoParaModificacion() {
        return this.documentosDeSustentoParaModificacion;
    }

    public void setDocumentoDeSustentoParaModificacion(File[] documentosDeSustentoParaModificacion) {
        this.documentosDeSustentoParaModificacion = documentosDeSustentoParaModificacion;
    }

    public String[] getDocumentoDeSustentoParaModificacionFileName() {
        return this.documentosDeSustentoParaModificacionFileNames;
    }

    public void setDocumentoDeSustentoParaModificacionFileName(String[] documentosDeSustentoParaModificacionFileNames) {
        this.documentosDeSustentoParaModificacionFileNames = documentosDeSustentoParaModificacionFileNames;
    }

    public String[] getDocumentoDeSustentoParaModificacionContentType() {
        return documentosDeSustentoParaModificacionContentTypes;
    }

    public void setDocumentoDeSustentoParaModificacionContentType(String[] documentosDeSustentoParaModificacionContentTypes) {
        this.documentosDeSustentoParaModificacionContentTypes = documentosDeSustentoParaModificacionContentTypes;
    }

    private ObligacionDePago obligacionDePago;

    public ObligacionDePago getObligacionDePago() {
        return obligacionDePago;
    }

    public void setObligacionDePago(ObligacionDePago obligacionDePago) {
        this.obligacionDePago = obligacionDePago;
    }

    @Override
    public String execute() throws Exception {
        if (esGet()) {
            obligacionDePago.recargar();
            return INPUT;
        } else {
            if (documentosDeSustentoParaModificacion != null) {
                for (int i = 0; i < documentosDeSustentoParaModificacion.length; i++) {
                    obligacionDePago.añadirDocumentoDeSustento(documentosDeSustentoParaModificacionFileNames[i], documentosDeSustentoParaModificacion[i]);
                }
            }
            obligacionDePago.actualizar();
            addActionMessage("Se modificó obligación de pago correctamente.");
            return SUCCESS;
        }

    }


    @Override
    public void validate() {
        if (esPost()) {
            String nuevaDescripcion = obligacionDePago.getDescripcion();
            if (StringUtils.isBlank(nuevaDescripcion)) {
                addFieldError("obligacionDePago.descripcion", "El motivo de pago/deuda es requerido.");
            }

            // FIXME código casi duplicado en pe.edu.cibertec.pandero.example.acciones.socios.GenerarObligacionDePago.
            if (documentosDeSustentoParaModificacionFileNames != null) {
                for (String nombreDeArchivoDeDocumentoDeSustento : documentosDeSustentoParaModificacionFileNames) {
                    String extension = FilenameUtils.getExtension(nombreDeArchivoDeDocumentoDeSustento).toLowerCase();
                    if (!extension.equals("pdf")) {
                        addFieldError("documentosDeSustentoParaModificacion", "Solo se admiten documentos con formato PDF.");
                    }
                }
            }

            try {
                // Se recarga el objeto para que en el caso de que la validación haya fallado se pueda mostrar el formulario en modo de error por validación.
                // TODO investigar: de repente esto se podría hacer nmediatamente después de que se haya realizado el "binding" del valor de obligacionDePago.codigo, utilizando algún mecanismo de Struts.
                obligacionDePago.recargar();
            } catch (IOException | ClassNotFoundException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
            // Restablecemos la nueva descripción, posiblemente vacía.
            obligacionDePago.setDescripcion(nuevaDescripcion);
        }

    }
}
