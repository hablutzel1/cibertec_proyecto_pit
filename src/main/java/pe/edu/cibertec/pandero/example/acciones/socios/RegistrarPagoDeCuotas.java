package pe.edu.cibertec.pandero.example.acciones.socios;

import org.apache.commons.lang3.StringUtils;
import pe.edu.cibertec.pandero.example.acciones.AccionBase;
import pe.edu.cibertec.pandero.example.modelo.Cuota;
import pe.edu.cibertec.pandero.example.modelo.ObligacionDePago;

import java.util.List;

public class RegistrarPagoDeCuotas extends AccionBase {

    private String codigoDeComprobanteDePago;
    private List<Integer> cuotasSeleccionadas;
    private ObligacionDePago obligacionDePago;

    @Override
    public String execute() throws Exception {
        // FIXME nótese que cada llamada a 'registrarPagoDeCuota' hace una llamada a 'org.apache.ibatis.session.SqlSession.commit()' y la transacción debería gestionarse al nivel de esta acción.
        for (Integer cuotasSeleccionada : cuotasSeleccionadas) {
            obligacionDePago.registrarPagoDeCuota(cuotasSeleccionada, codigoDeComprobanteDePago);
        }
        return SUCCESS;
    }

    @Override
    public void validate() {
        if (StringUtils.isBlank(codigoDeComprobanteDePago)) {
            addFieldError("codigoDeComprobanteDePago", "El código de comprobante de pago es requerido.");
        }
        if (cuotasSeleccionadas == null) {
            addFieldError("cuotasSeleccionadas", "Se debe seleccionar al menos una cuota a pagar.");
        } else {
            // TODO validar que las cuotas seleccionadas no hayan sido pagadas antes, aunque la interfaz de usuario previene de esto al no mostrar las cajas de selección para las cuotas ya pagadas, pero igual, podrían haber dos ventanas del navegador abiertas.

            // TODO evaluar realizar esta validación del negocio al nivel del modelo. En vez de hacer llamadas individuales a "pe.edu.cibertec.pandero.example.modelo.ObligacionDePago#registrarPagoDeCuota(Integer numeroDeCuota, String codigoDeComprobanteDePago)" se debería llamar a un método que reciba la colección de cuotas por pagar para que tenga la oportunidad de hacer esta validación. Sin embargo, en esta caso habría que considerar mover todas las validaciones que suceden en los métodos 'pe.edu.cibertec.pandero.example.acciones.socios.RegistrarPagoDeCuotas.validate' al modelo y aparentemente para no repetirlas se tendría que controlar las excepciones producto de estas validaciones (ya en el modelo) desde los propios métodos "execute" en vez del método "validate" de Struts 2.
            //region validar que en todos los casos se estén pagando las cuotas antiguas primero.
            int primeraCuotaSinPagar = -1;
            List<Cuota> todasLasCuotas = obligacionDePago.getCuotas();
            for (Cuota cuota : todasLasCuotas) {
                if (cuota.getFechaPago() == null) {
                    primeraCuotaSinPagar = cuota.getCodigo();
                    break;
                }
            }
            for (Integer cuotaSeleccionada : cuotasSeleccionadas) {
                if (!(cuotaSeleccionada == primeraCuotaSinPagar || cuotasSeleccionadas.contains(cuotaSeleccionada - 1))) {
                    addFieldError("cuotasSeleccionadas", "Siempre se deben cancelar primero las cuotas antiguas.");
                    break;
                }
            }
            //endregion

        }
    }

    public ObligacionDePago getObligacionDePago() {
        return obligacionDePago;
    }

    public void setObligacionDePago(ObligacionDePago obligacionDePago) {
        this.obligacionDePago = obligacionDePago;
    }

    public String getCodigoDeComprobanteDePago() {
        return codigoDeComprobanteDePago;
    }

    public void setCodigoDeComprobanteDePago(String codigoDeComprobanteDePago) {
        this.codigoDeComprobanteDePago = codigoDeComprobanteDePago;
    }

    public List<Integer> getCuotasSeleccionadas() {
        return cuotasSeleccionadas;
    }

    public void setCuotasSeleccionadas(List<Integer> cuotasSeleccionadas) {
        this.cuotasSeleccionadas = cuotasSeleccionadas;
    }
}
