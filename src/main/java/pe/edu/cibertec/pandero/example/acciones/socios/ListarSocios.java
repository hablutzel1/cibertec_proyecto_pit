package pe.edu.cibertec.pandero.example.acciones.socios;

import com.opensymphony.xwork2.ActionSupport;
import pe.edu.cibertec.pandero.example.modelo.Socio;

import java.util.List;

public class ListarSocios extends ActionSupport {

    private List<Socio> todosLosSocios;

    @Override
    public String execute() throws Exception {
        todosLosSocios = Socio.todos();
        return super.execute();
    }

    public List<Socio> getTodosLosSocios() {
        return todosLosSocios;
    }

    public void setTodosLosSocios(List<Socio> todosLosSocios) {
        this.todosLosSocios = todosLosSocios;
    }
}
