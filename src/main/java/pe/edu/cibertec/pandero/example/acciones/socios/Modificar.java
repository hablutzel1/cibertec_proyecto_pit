package pe.edu.cibertec.pandero.example.acciones.socios;

import org.apache.commons.lang3.StringUtils;
import pe.edu.cibertec.pandero.example.acciones.AccionBase;
import pe.edu.cibertec.pandero.example.modelo.Socio;

public class Modificar extends AccionBase {

    private Socio socio;

    @Override
    public String execute() throws Exception {
        verificarPerfil("administrador");
        if (esGet()) {
            this.socio = Socio.buscarPorCodigo(socio.getCodigo());
            return INPUT;
        } else {
            this.socio.registrarModificacion();
            // TODO mostrar mensaje de éxito.
            return SUCCESS;
        }
    }

    @Override
    public void validate() { // NOTE nótese que este método se ejecuta antes que "execute", por lo tanto aún no se habría realizado la verificación del perfil.
        if (esPost()) {
            if (StringUtils.isBlank(socio.getDireccion())) {
                addFieldError("socio.direccion", "La dirección es requerida.");
            }
        }
    }


    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

}