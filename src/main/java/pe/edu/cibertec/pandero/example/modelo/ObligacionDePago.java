package pe.edu.cibertec.pandero.example.modelo;

import org.apache.commons.io.FileUtils;
import org.apache.ibatis.session.SqlSession;
import org.joda.time.DateTime;
import org.joda.time.Weeks;

import java.io.*;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ObligacionDePago extends UnidadDePersistencia {

    private int codigo;
    private String descripcion;
    private double monto;
    private int numeroDeCuotas;
    private Socio socio;
    private Date fechaDeEmision;

    public boolean isCuotasRepetitivas() {
        return cuotasRepetitivas;
    }

    public void setCuotasRepetitivas(boolean cuotasRepetitivas) {
        this.cuotasRepetitivas = cuotasRepetitivas;
    }

    private boolean cuotasRepetitivas;

    // FIXME no es el API más apropiado, Map<String, File> estaría mejor probablemente.
    public Map<String, byte[]> getDocumentosDeSustento() {
        return documentosDeSustento;
    }

    private Map<String, byte[]> documentosDeSustento = new LinkedHashMap<>();

    private byte[] documentosDeSustentoSerializados;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Puede ser el monto total de la obligación de pago o el monto por cuota en el caso de las obligaciones de pago con cuotas repetitivas.
     */
    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public int getNumeroDeCuotas() {
        // TODO evaluar persistir pe.edu.cibertec.pandero.example.modelo.ObligacionDePago.cuotasRepetitivas para no complicar la lógica alrededor del atributo numeroDeCuotas.
        if (cuotasRepetitivas) {
            return -1;
        }
        return numeroDeCuotas;
    }

    public void setNumeroDeCuotas(int numeroDeCuotas) {
        this.numeroDeCuotas = numeroDeCuotas;
    }

    public int getNumeroDeCuotasPendientesDePago() {
        List<Cuota> todasLasCuotas = getCuotas();
        int cuotasPendientesDePago = 0;
        for (Cuota cuota : todasLasCuotas) {
            if (cuota.getFechaPago() == null) {
                cuotasPendientesDePago++;
            }
        }
        return cuotasPendientesDePago;
    }

    // TODO derepente lo más orientado a objetos sería un método de instancia 'registrarObligacionDePago' para la clase Socio.
    public void generar() throws IOException {
        try (SqlSession sqlSession = getSqlSession()) {
            DateTime hoy = new DateTime();
            fechaDeEmision = hoy.toDate();
            serializarDocumentosDeSustento();
            sqlSession.insert("pe.edu.cibertec.pandero.example.modelo.ObligacionDePagoMapper.guardar", this);
            if (!cuotasRepetitivas) {
                double montoPorCuota = monto / numeroDeCuotas;
                for (int i = 1; i <= numeroDeCuotas; i++) {
                    Cuota cuota = new Cuota();
                    cuota.setObligacionDePago(this);
                    cuota.setCodigo(i);
                    // TODO revisar qué día escogerá joda-time para los próximos meses cuando se sume un mes y hoy sea, por ejemplo, 31, considerando que algunos meses tienen menos días.
                    cuota.setFechaVencimiento(hoy.plusMonths(i).toDate());
                    cuota.setMonto(montoPorCuota);
                    sqlSession.insert("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.guardar", cuota);
                }
            } else {
                Cuota primeraCuota = new Cuota();
                primeraCuota.setObligacionDePago(this);
                primeraCuota.setCodigo(1);
                primeraCuota.setFechaVencimiento(new DateTime(getFechaDeEmision()).plusMonths(1).toDate());
                double montoPorCuota = this.monto;
                primeraCuota.setMonto(montoPorCuota);
                sqlSession.insert("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.guardar", primeraCuota);
            }
            sqlSession.commit(); // TODO asegurarse de que este método se esté ejecutando de manera transaccional debido a esta llamada a 'commit'.
        }
    }


    /**
     * Genera, de ser necesario, nuevas cuotas para las obligaciones de pago con cuotas repetitivas.
     */
    public static void generarCuotasRepetitivasDeSerNecesario() {
        try (SqlSession sqlSession = getSqlSession()) {
            List<ObligacionDePago> todasLasObligacionesDePagoConCuotasRepetitivas = sqlSession.selectList("pe.edu.cibertec.pandero.example.modelo.ObligacionDePagoMapper.todasLasObligacionesConCuotasRepetitivas");
            for (ObligacionDePago obligacionDePago : todasLasObligacionesDePagoConCuotasRepetitivas) {
                List<Cuota> cuotasActuales = obligacionDePago.getCuotas();
                Cuota ultimaCuota = cuotasActuales.get(cuotasActuales.size() - 1);
                DateTime ahora = DateTime.now();
                DateTime vencimientoDeLaUltimaCuota = new DateTime(ultimaCuota.getFechaVencimiento().getTime());
                int semanasHastaElVencimientoDeLaUltimaCuota = Weeks.weeksBetween(ahora, vencimientoDeLaUltimaCuota).getWeeks();
                // TODO revisar: si la fecha actual es mayor por más de un mes que la fecha de la última cuota (ej. si el servidor estuvo apagado por más de un mes) entonces derepente después de crear una cuota se debería volver a ejecutar esta tarea para que se cree probablamente una cuota más, pues la primera cuota que se generaría ya podría estar a menos de una semana de vencer, y esto quizá se debería ejecutar de manera recursiva hasta que no hayan más cuotas que generar.
                if (semanasHastaElVencimientoDeLaUltimaCuota < 1) { // Se crea una nueva cuota si la última está a menos de una semana de vencer.
                    Cuota nuevaCuotaRepetitiva = new Cuota();
                    nuevaCuotaRepetitiva.setObligacionDePago(obligacionDePago);
                    nuevaCuotaRepetitiva.setCodigo(ultimaCuota.getCodigo() + 1);
                    // TODO revisar la veracidad de esto: Si la fecha de emisión era el 31/01, la fecha de vencimiento de la primera cuota sería 28/02 no es así? y a partir de eso la tercera cuota sería el 28/03 y no el 30/03 como se esperaría si la adición de meses en todos los casos se hace a partir de la fecha de emisión como en el caso de cuotas normales no repetitivas... quizás en ese caso convendría seguir calculando la fecha de vencimiento de las nuevas cuotas a partir de la fecha de emisión original no es cierto?.
                    nuevaCuotaRepetitiva.setFechaVencimiento(new DateTime(ultimaCuota.getFechaVencimiento()).plusMonths(1).toDate());
                    nuevaCuotaRepetitiva.setMonto(obligacionDePago.getMonto());
                    sqlSession.insert("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.guardar", nuevaCuotaRepetitiva);
                }
            }
            sqlSession.commit();
        }
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return la lista de cuotas ordenadas de manera ascendente por el código o número de cuota
     */
    // TODO evaluar guardar en la memoria caché durante la recarga del objeto o de manera perezosa después de la primera llamada, aunque esto podría generar el problema de vistas desactualizadas de la información, ¿no es cierto?.
    public List<Cuota> getCuotas() {
        try (SqlSession sqlSession = getSqlSession()) {
            return sqlSession.selectList("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.buscarPorObligacion", codigo);
        }
    }

    public void recargar() throws IOException, ClassNotFoundException {
        try (SqlSession sqlSession = getSqlSession()) {
            ObligacionDePago obligacionDePagoDeBD = sqlSession.selectOne("pe.edu.cibertec.pandero.example.modelo.ObligacionDePagoMapper.obtener", codigo);
            this.descripcion = obligacionDePagoDeBD.descripcion;
            this.monto = obligacionDePagoDeBD.monto;
            this.numeroDeCuotas = obligacionDePagoDeBD.numeroDeCuotas;
            this.fechaDeEmision = obligacionDePagoDeBD.fechaDeEmision;
            this.documentosDeSustentoSerializados = obligacionDePagoDeBD.documentosDeSustentoSerializados;
            deserializarDocumentosDeSustento();
            this.socio = Socio.buscarPorCodigo(obligacionDePagoDeBD.getSocio().getCodigo());
        }
    }


    private void serializarDocumentosDeSustento() throws IOException {
        ByteArrayOutputStream flujoDeSalidaDeBytes = new ByteArrayOutputStream();
        ObjectOutputStream flujoDeSalidaDeObjetos = new ObjectOutputStream(flujoDeSalidaDeBytes);
        flujoDeSalidaDeObjetos.writeObject(documentosDeSustento);
        flujoDeSalidaDeObjetos.flush();
        flujoDeSalidaDeObjetos.close();
        documentosDeSustentoSerializados = flujoDeSalidaDeBytes.toByteArray();
    }

    @SuppressWarnings("unchecked")
    private void deserializarDocumentosDeSustento() throws IOException, ClassNotFoundException {
        ObjectInputStream flujoDeEntradaDeObjectos = new ObjectInputStream(new ByteArrayInputStream(this.documentosDeSustentoSerializados));
        this.documentosDeSustento = (Map<String, byte[]>) flujoDeEntradaDeObjectos.readObject();
        // TODO revisar: se debería cerrar el flujoDeEntradaDeObjetos?.
    }

    public void registrarPagoDeCuota(Integer numeroDeCuota, String codigoDeComprobanteDePago) {
        Cuota cuota = new Cuota();
        cuota.setObligacionDePago(this);
        cuota.setCodigo(numeroDeCuota);
        cuota.recargar();
        // Importante que la llamada a 'Cuota.getInteresesALaFecha' se haga antes de llamar a 'Cuota.setFechaPago'.
        double interesesALaFecha = cuota.getInteresesALaFecha();
        double totalAPagar = cuota.getMonto();
        if (interesesALaFecha != -1) {
            totalAPagar += interesesALaFecha;
        }
        Date ahora = new Date();
        cuota.setFechaPago(ahora);
        cuota.setComprobanteDePago(codigoDeComprobanteDePago);
        cuota.setMontoPagado(totalAPagar);
        try (SqlSession sqlSession = getSqlSession()) {
            sqlSession.update("pe.edu.cibertec.pandero.example.modelo.CuotaMapper.registrarPago", cuota);
            sqlSession.commit();
        }
    }

    public Date getFechaDeEmision() {
        return fechaDeEmision;
    }

    public void setFechaDeEmision(Date fechaDeEmision) {
        this.fechaDeEmision = fechaDeEmision;
    }

    public void añadirDocumentoDeSustento(String nombreDeDocumento, File documento) throws IOException {
        // NOTE que se trata de un Map que no puede tener entradas con la misma clave, en este caso, con el mismo nombre de archivo. TODO revisar las implicaciones concretas en este caso.
        documentosDeSustento.put(nombreDeDocumento, FileUtils.readFileToByteArray(documento));
    }

    public byte[] getDocumentosDeSustentoSerializados() {
        return documentosDeSustentoSerializados;
    }

    public void setDocumentosDeSustentoSerializados(byte[] documentosDeSustentoSerializados) {
        this.documentosDeSustentoSerializados = documentosDeSustentoSerializados;
    }

    /**
     * Actualizar solamente la descripción y los documentos de sustento.
     */
    public void actualizar() throws IOException {
        try (SqlSession sqlSession = getSqlSession()) {
            serializarDocumentosDeSustento();
            sqlSession.insert("pe.edu.cibertec.pandero.example.modelo.ObligacionDePagoMapper.actualizar", this);
            sqlSession.commit();
        }

    }
}
