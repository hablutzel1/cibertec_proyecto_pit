package pe.edu.cibertec.pandero.example.acciones.socios;

import com.opensymphony.xwork2.ActionSupport;
import pe.edu.cibertec.pandero.example.modelo.Socio;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class VerDetalle extends ActionSupport {

    private Socio socio;
    private InputStream flujoDeImagen;

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    @Override
    public String execute() throws Exception {
        popularSocio();
        return super.execute();
    }

    public String imagen() throws  Exception {
        popularSocio();
        this.flujoDeImagen = new ByteArrayInputStream(socio.getImagen());
        return SUCCESS;
    }

    public String huella() throws  Exception {
        popularSocio();
        this.flujoDeImagen = new ByteArrayInputStream(socio.getHuella());
        return SUCCESS;
    }

    public String firma() throws  Exception {
        popularSocio();
        this.flujoDeImagen = new ByteArrayInputStream(socio.getFirma());
        return SUCCESS;
    }

    private void popularSocio() {
        this.socio = Socio.buscarPorCodigo(socio.getCodigo());
    }

    public InputStream getFlujoDeImagen() {
        return flujoDeImagen;
    }

    public void setFlujoDeImagen(InputStream flujoDeImagen) {
        this.flujoDeImagen = flujoDeImagen;
    }
}