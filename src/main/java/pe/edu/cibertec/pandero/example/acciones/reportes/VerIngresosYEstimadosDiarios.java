package pe.edu.cibertec.pandero.example.acciones.reportes;

import pe.edu.cibertec.pandero.example.modelo.Cuota;

import java.util.Date;
import java.util.List;

public class VerIngresosYEstimadosDiarios extends AccionBaseDeReportes {

    private Date fecha;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public String execute() throws Exception {
        return super.execute();
    }

    @Override
    public void validate() {
        if (esPost()) {
            if (fecha == null) {
                addFieldError("fecha", "La fecha es requerida.");
            }
        }
    }

    public String reporteestimadosdiarios() throws Exception {
        List<Cuota> cuotas = Cuota.obtenerCuotasSinPagar(fecha);
        if (cuotas.isEmpty()) {
            return NOHAYDATOS;
        }
        fuenteDeDatos = AccionBaseDeReportes.transformarAFuenteDeDatos(cuotas);
        return "estimadosdiarios";

    }

    public String reporteingresosdiarios() throws Exception {
        List<Cuota> cuotas = Cuota.obtenerCuotasPagadas(fecha);
        if (cuotas.isEmpty()) {
            return NOHAYDATOS;
        }
        fuenteDeDatos = AccionBaseDeReportes.transformarAFuenteDeDatos(cuotas);
        return "ingresosdiarios";

    }

}