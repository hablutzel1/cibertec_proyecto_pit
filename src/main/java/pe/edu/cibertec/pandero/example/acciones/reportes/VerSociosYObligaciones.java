package pe.edu.cibertec.pandero.example.acciones.reportes;

import pe.edu.cibertec.pandero.example.modelo.ObligacionDePago;
import pe.edu.cibertec.pandero.example.modelo.Socio;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class VerSociosYObligaciones extends AccionBaseDeReportes {

    @Override
    public String execute() throws Exception {
        return super.execute();
    }

    public String reporte() throws Exception {
        fuenteDeDatos = new ArrayList<>();
        List<Socio> socios = Socio.todos();
        for (Socio socio : socios) {
            List<ObligacionDePago> obligacionesDePago = socio.getObligacionesDePago();
            for (ObligacionDePago obligacionDePago : obligacionesDePago) {
                HashMap<String, Object> e = new HashMap<>();
                e.put("codigo", socio.getCodigo());
                e.put("dni", socio.getDni());
                e.put("nombres", socio.getNombres());
                e.put("apellidos", socio.getApellidos());
                e.put("descripcion", obligacionDePago.getDescripcion());
                e.put("monto", BigDecimal.valueOf(obligacionDePago.getMonto()));
                e.put("num_cuotas", obligacionDePago.getNumeroDeCuotas());
                fuenteDeDatos.add(e);
            }
        }
        if (fuenteDeDatos.isEmpty()) {
            return NOHAYDATOS;
        }
        return "reporte";
    }
}