package pe.edu.cibertec.pandero.example.acciones.socios;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.Period;
import pe.edu.cibertec.pandero.example.modelo.Socio;

import java.io.File;
import java.util.Date;

public class RegistrarSocio extends ActionSupport {

    public static final String RUTA_DIRECTORIO_HUELLAS = "C:\\_huellas";

    private Socio socio;

    @Override
    public String execute() throws Exception {
        if (socio == null) {
            return INPUT;
        } else {

            // Estableciendo la huella a partir del archivo que se espera en una determinada ruta.
            // TODO evaluar mover esto al modelo, pero, ¿realmente el modelo se debería enterar de 'RUTA_DIRECTORIO_HUELLAS'?
            File[] archivosEnCarpetaDeHuellas = new File(RUTA_DIRECTORIO_HUELLAS).listFiles();
            // TODO evaluar validar que el formato de la huella sea PNG, nótese que la acción "VerDetalle_*" de Struts 2 está indicando PNG siempre como el 'contentType'.
            socio.setHuella(FileUtils.readFileToByteArray(archivosEnCarpetaDeHuellas[0]));
            archivosEnCarpetaDeHuellas[0].delete();

            socio.registrarNuevo();
            return SUCCESS;
        }
    }

    @Override
    public void validate() {
        if (socio != null) {

            final String dni = socio.getDni();
            if (StringUtils.isBlank(dni)){
                addFieldError("socio.dni", "El DNI es requerido.");
            } else if(!StringUtils.isNumeric(dni) || StringUtils.length(dni) != 8){
                addFieldError("socio.dni", "El DNI debe tener 8 carácteres numéricos.");
            } else {
                Socio socio = Socio.buscarPorDNI(dni);
                if (socio != null) {
                    addFieldError("socio.dni", "El DNI ya está siendo usado para otro socio.");
                }
            }

            if (StringUtils.isBlank(socio.getNombres())){
                addFieldError("socio.nombres", "Los nombres son requeridos.");
            }

            if (!StringUtils.isAlphaSpace(socio.getNombres())){
                addFieldError("socio.nombres", "Los nombres deberían contener solo letras y espacios.");
            }

            if (StringUtils.isBlank(socio.getApellidos())){
                addFieldError("socio.apellidos", "Los apellidos son requeridos.");
            }

            if (!StringUtils.isAlphaSpace(socio.getApellidos())){
                addFieldError("socio.apellidos", "Los apellidos deberían contener solo letras y espacios.");
            }

            if (socio.getFechaNacimiento() == null) {
                addFieldError("socio.fechaNacimiento", "La fecha de nacimiento es requerida.");
            } else if (new Period(socio.getFechaNacimiento().getTime(), new Date().getTime()).getYears() < 18) { // TODO revisar: de repente se debería usar org.joda.time.Years.yearsBetween(org.joda.time.ReadableInstant, org.joda.time.ReadableInstant). Revisar esto porque se observó un comportamiento extraño en el caso de org.joda.time.Period.getWeeks(), cuando las semanas de diferencia eran mayores que cuatro o cinco, en esos casos aquel metodo estaba devolviendo 0.
                addFieldError("socio.fechaNacimiento", "Se requiere ser mayor de edad para convertirse en socio.");
            }

            if (StringUtils.isBlank(socio.getTelefono())){
                addFieldError("socio.telefono", "El teléfono es requerido.");
            }

            if (StringUtils.isBlank(socio.getDireccion())){
                addFieldError("socio.direccion", "La dirección es requerida.");
            }

            if (StringUtils.isBlank(socio.getImagenComoB64())){
                addFieldError("socio.imagenComoB64", "Es necesario capturar la imagen.");
            }

            File file = new File(RUTA_DIRECTORIO_HUELLAS);
            if (!file.exists()){
                addFieldError("socio.huella", "No existe la carpeta " + RUTA_DIRECTORIO_HUELLAS + ".");
            } else if (file.listFiles().length != 1) {
                addFieldError("socio.huella", "En el directorio " + RUTA_DIRECTORIO_HUELLAS + " debe existir un único archivo correspondiente a la huella dactilar.");
            }

            if (StringUtils.isBlank(socio.getFirmaComoB64())){
                addFieldError("socio.firmaComoB64", "Es necesario proveer la firma manuscrita.");
            }
        }
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }
}
