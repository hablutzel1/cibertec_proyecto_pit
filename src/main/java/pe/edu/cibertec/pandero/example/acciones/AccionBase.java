package pe.edu.cibertec.pandero.example.acciones;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import pe.edu.cibertec.pandero.example.modelo.Usuario;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public abstract class AccionBase extends ActionSupport implements SessionAware, ServletRequestAware {

    protected HttpServletRequest solicitud;
    protected Map<String, Object> sesion;

    protected void verificarPerfil(String perfilEsperado) {
        if (!((Usuario) sesion.get("usuario")).getPerfil().equals(perfilEsperado)) {
            throw new SecurityException("Usted no posee el perfil \"" + perfilEsperado + "\".");
        }
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.sesion = session;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        this.solicitud = request;
    }

    protected boolean esGet() {
        return solicitud.getMethod().equals("GET");
    }

    protected boolean esPost() {
        return solicitud.getMethod().equals("POST");
    }
}
