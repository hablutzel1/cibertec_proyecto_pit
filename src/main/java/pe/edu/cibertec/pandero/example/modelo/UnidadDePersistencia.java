package pe.edu.cibertec.pandero.example.modelo;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;

/**
 * Basicamente se asegura de que se cree la base de datos 'bd_pandero' si esta aún no existe.
 */
abstract class UnidadDePersistencia {

    static {
        try {
            crearBaseDeDatosSiNoExiste();
        } catch (ClassNotFoundException | IOException | SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    protected static SqlSession getSqlSession() {
        // TODO determinar si aquí hay alguna operación bastante costosa que justifique crear un "singleton".
        Reader reader;
        try {
            reader = Resources.getResourceAsReader("mybatis-config.xml");
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return new SqlSessionFactoryBuilder().build(reader).openSession();
    }

    private static void crearBaseDeDatosSiNoExiste() throws ClassNotFoundException, IOException, SQLException {
        //  Si la base de datos no existe, se crea a partir de 'bd_pandero.sql'.
        Class.forName("com.mysql.jdbc.Driver");
        Properties databaseProperties = new Properties();
        databaseProperties.load(UnidadDePersistencia.class.getResourceAsStream("/database.properties"));
        Connection connection = DriverManager.getConnection(databaseProperties.getProperty("jdbc.baseurl"), databaseProperties.getProperty("username"), databaseProperties.getProperty("password"));
        boolean databaseExists = false;
        ResultSet resultSet = connection.getMetaData().getCatalogs();
        while (resultSet.next()) {
            String databaseName = resultSet.getString(1);
            if (databaseName.equals(databaseProperties.getProperty("database"))) {
                databaseExists = true;
            }
        }
        resultSet.close();
        if (!databaseExists) {
            importarSQL(connection, UnidadDePersistencia.class.getResourceAsStream("/bd_pandero.sql"));
        }
    }

    private static void importarSQL(Connection conn, InputStream in) throws SQLException {
        Scanner s = new Scanner(in);
        s.useDelimiter("(;(\r)?\n)|(--\n)");
        Statement st = null;
        try {
            st = conn.createStatement();
            while (s.hasNext()) {
                String line = s.next();
                if (line.startsWith("/*!") && line.endsWith("*/")) {
                    int i = line.indexOf(' ');
                    line = line.substring(i + 1, line.length() - " */".length());
                }

                if (line.trim().length() > 0) {
                    st.execute(line);
                }
            }
        } finally {
            if (st != null) st.close();
        }
    }
}
