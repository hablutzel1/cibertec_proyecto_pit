package pe.edu.cibertec.pandero.example.acciones.usuarios;

import org.apache.commons.lang3.StringUtils;
import pe.edu.cibertec.pandero.example.acciones.AccionBase;
import pe.edu.cibertec.pandero.example.modelo.Usuario;

public class RegistrarUsuario extends AccionBase {

    private Usuario usuario;

    @Override
    public String execute() throws Exception {
        verificarPerfil("administrador");
        if (usuario == null) {
            return INPUT;
        } else {
            usuario.guardar();
            return SUCCESS;
        }
    }

    @Override
    public void validate() {
        if (usuario != null) {
            final String nombreUsuario = usuario.getNombreUsuario();
            if (StringUtils.isBlank(nombreUsuario)) {
                addFieldError("usuario.nombreUsuario", "El nombre de usuario es requerido.");
            } else if (!StringUtils.isAlphanumeric(nombreUsuario)) {
                addFieldError("usuario.nombreUsuario", "El nombre de usuario debe ser alfanumérico");
            } else {
                Usuario usuario = Usuario.buscar(nombreUsuario);
                if (usuario != null) {
                    addFieldError("usuario.nombreUsuario", "El nombre de usuario ya está siendo utilizado.");
                }
            }

            if (StringUtils.isBlank(usuario.getContrasena())) {
                addFieldError("usuario.contrasena", "La contraseña es requerida.");
            }
        }
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
