package pe.edu.cibertec.pandero.example.modelo;

import org.apache.ibatis.session.SqlSession;

import java.util.List;

public class Usuario extends UnidadDePersistencia {
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    private String nombreUsuario;

    // TODO evaluar persistir solamente un resumen.
    private String contrasena;

    private String perfil;

    public void guardar() {
        try (SqlSession sqlSession = getSqlSession()) {
            sqlSession.insert("pe.edu.cibertec.pandero.example.modelo.UsuarioMapper.insertar", this);
            sqlSession.commit();
        }
    }

    public static Usuario buscar(String nombreUsuario) {
        try (SqlSession sqlSession = getSqlSession()) {
            return sqlSession.selectOne("pe.edu.cibertec.pandero.example.modelo.UsuarioMapper.buscar", nombreUsuario);
        }
    }

    public static List<Usuario> todos() {
        try (SqlSession sqlSession = getSqlSession()) {
            return sqlSession.selectList("pe.edu.cibertec.pandero.example.modelo.UsuarioMapper.todos");
        }
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
}
