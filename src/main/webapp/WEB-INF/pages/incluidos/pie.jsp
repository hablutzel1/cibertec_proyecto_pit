<%@ page pageEncoding="UTF-8" %>

</div>
<!-- end: Content -->

</div><!--/row-->

</div><!--/container-->


<div class="modal fade hidden-print" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>Here settings can be configured...</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="clearfix"></div>

<footer class="hidden-print">

    <div class="row">

        <div class="col-sm-5">
            &copy; 2014 creativeLabs. <a href="http://bootstrapmaster.com">Admin Templates</a> by BootstrapMaster
        </div><!--/.col-->

        <div class="col-sm-7 text-right">
            Powered by: <a href="http://bootstrapmaster.com/demo/simpliq/" alt="Bootstrap Admin Templates">SimpliQ
            Dashboard</a> | Based on Bootstrap 3.1.1 | Built with brix.io <a href="http://brix.io"
                                                                             alt="Brix.io - Interface Builder">Interface
            Builder</a>
        </div><!--/.col-->

    </div><!--/.row-->

</footer>

<!-- start: JavaScript-->
<!--[if !IE]>-->

<script src="<s:url value="/assets/js/jquery-2.1.0.min.js"/>"></script>

<!--<![endif]-->

<!--[if IE]>

<script src="<s:url value="/assets/js/jquery-1.11.0.min.js"/>"></script>

<![endif]-->

<!--[if !IE]>-->

<script type="text/javascript">
    window.jQuery || document.write("<script src='<s:url value="/assets/js/jquery-2.1.0.min.js"/>'>" + "<" + "/script>");
</script>

<!--<![endif]-->

<!--[if IE]>

<script type="text/javascript">
window.jQuery || document.write("<script src='<s:url value="/assets/js/jquery-1.11.0.min.js"/>'>"+"<"+"/script>");
</script>

<![endif]-->
<script src="<s:url value="/assets/js/jquery-migrate-1.2.1.min.js"/>"></script>
<script src="<s:url value="/assets/js/bootstrap.min.js"/>"></script>


<!-- page scripts -->
<script src="<s:url value="/assets/js/jquery-ui-1.10.3.custom.min.js"/>"></script>
<script src="<s:url value="/assets/js/jquery.sparkline.min.js"/>"></script>
<script src="<s:url value="/assets/js/jquery.chosen.min.js"/>"></script>
<script src="<s:url value="/assets/js/jquery.cleditor.min.js"/>"></script>
<script src="<s:url value="/assets/js/jquery.autosize.min.js"/>"></script>
<script src="<s:url value="/assets/js/jquery.placeholder.min.js"/>"></script>
<script src="<s:url value="/assets/js/jquery.maskedinput.min.js"/>"></script>
<script src="<s:url value="/assets/js/jquery.inputlimiter.1.3.1.min.js"/>"></script>
<script src="<s:url value="/assets/js/bootstrap-datepicker.min.js"/>"></script>
<script src="<s:url value="/assets/js/bootstrap-timepicker.min.js"/>"></script>
<script src="<s:url value="/assets/js/moment.min.js"/>"></script>
<script src="<s:url value="/assets/js/daterangepicker.min.js"/>"></script>
<script src="<s:url value="/assets/js/jquery.hotkeys.min.js"/>"></script>
<script src="<s:url value="/assets/js/bootstrap-wysiwyg.min.js"/>"></script>
<script src="<s:url value="/assets/js/bootstrap-colorpicker.min.js"/>"></script>

<!-- theme scripts -->
<script src="<s:url value="/assets/js/custom.min.js"/>"></script>
<script src="<s:url value="/assets/js/core.min.js"/>"></script>

<!-- inline scripts related to this page -->
<%--<script src="<s:url value="/assets/js/pages/form-elements.js"/>"></script>--%>

<s:property value="jSPieDePagina" escapeHtml="false"/>
<!-- end: JavaScript-->

</body>
</html>