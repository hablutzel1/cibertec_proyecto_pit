<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="pandero" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>

    <!-- start: Meta -->
    <meta charset="utf-8">
    <title>Pandero - PIT</title>
    <meta name="description" content="SimpliQ - Flat & Responsive Bootstrap Admin Template.">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="SimpliQ, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <!-- end: Meta -->

    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->

    <!-- start: CSS -->
    <link href="<s:url value="/assets/css/bootstrap.min.css"/>" rel="stylesheet">
    <link href="<s:url value="/assets/css/style.css"/>" rel="stylesheet">
    <link href="<s:url value="/assets/css/retina.min.css"/>" rel="stylesheet">
    <!-- end: CSS -->


    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>

    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="<s:url value="/assets/js/respond.min.js"/>"></script>
    <script src="<s:url value="/assets/css/ie6-8.css"/>"></script>

    <![endif]-->

    <!-- start: Favicon and Touch Icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<s:url value="/assets/ico/apple-touch-icon-144-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<s:url value="/assets/ico/apple-touch-icon-114-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<s:url value="/assets/ico/apple-touch-icon-72-precomposed.png"/>">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="<s:url value="/assets/ico/apple-touch-icon-57-precomposed.png"/>">
    <link rel="shortcut icon" href="<s:url value="/assets/ico/favicon.png"/>">
    <!-- end: Favicon and Touch Icons -->

</head>

<body>

<s:if test="%{#session.usuario != null}">

<style type="text/css">
    .container #content {
        padding: 30px;
        margin: 0;
        height: 100%;
        width: 85.578%;
    }
</style>


<!-- start: Header -->
<header class="navbar hidden-print">
    <div class="container">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".sidebar-nav.nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a id="main-menu-toggle" class="hidden-xs open"><i class="fa fa-bars"></i></a>
        <a class="navbar-brand col-lg-2 col-sm-1 col-xs-12" href="<s:url action="index"/>"><span>Pandero - PIT</span></a>
        <!-- start: Header Menu -->
        <div class="nav-no-collapse header-nav">
            <ul class="nav navbar-nav pull-right">
                <li class="dropdown hidden-xs">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="form-elements.html#">
                        <i class="fa fa-warning"></i>
                    </a>
                    <ul class="dropdown-menu notifications">
                        <li class="dropdown-menu-title">
                            <span>You have 11 notifications</span>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="icon blue"><i class="fa fa-user"></i></span>
                                <span class="message">New user registration</span>
                                <span class="time">1 min</span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="icon green"><i class="fa fa-comment"></i></span>
                                <span class="message">New comment</span>
                                <span class="time">7 min</span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="icon green"><i class="fa fa-comment"></i></span>
                                <span class="message">New comment</span>
                                <span class="time">8 min</span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="icon green"><i class="fa fa-comment"></i></span>
                                <span class="message">New comment</span>
                                <span class="time">16 min</span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="icon blue"><i class="fa fa-user"></i></span>
                                <span class="message">New user registration</span>
                                <span class="time">36 min</span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="icon yellow"><i class="fa fa-shopping-cart"></i></span>
                                <span class="message">2 items sold</span>
                                <span class="time">1 hour</span>
                            </a>
                        </li>
                        <li class="warning">
                            <a href="form-elements.html#">
                                <span class="icon red"><i class="fa fa-user"></i></span>
                                <span class="message">User deleted account</span>
                                <span class="time">2 hour</span>
                            </a>
                        </li>
                        <li class="warning">
                            <a href="form-elements.html#">
                                <span class="icon red"><i class="fa fa-shopping-cart"></i></span>
                                <span class="message">Transaction was canceled</span>
                                <span class="time">6 hour</span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="icon green"><i class="fa fa-comment"></i></span>
                                <span class="message">New comment</span>
                                <span class="time">yesterday</span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="icon blue"><i class="fa fa-user"></i></span>
                                <span class="message">New user registration</span>
                                <span class="time">yesterday</span>
                            </a>
                        </li>
                        <li class="dropdown-menu-sub-footer">
                            <a>View all notifications</a>
                        </li>
                    </ul>
                </li>
                <!-- start: Notifications Dropdown -->
                <li class="dropdown hidden-xs">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="form-elements.html#">
                        <i class="fa fa-tasks"></i>
                    </a>
                    <ul class="dropdown-menu tasks">
                        <li>
                            <span class="dropdown-menu-title">You have 17 tasks in progress</span>
                        </li>
                        <li>
                            <a href="form-elements.html#">
									<span class="header">
										<span class="title">iOS Development</span>
										<span class="percent"></span>
									</span>
                                <div class="taskProgress progressSlim progressBlue">80</div>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
									<span class="header">
										<span class="title">Android Development</span>
										<span class="percent"></span>
									</span>
                                <div class="taskProgress progressSlim progressYellow">47</div>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
									<span class="header">
										<span class="title">Django Project For Google</span>
										<span class="percent"></span>
									</span>
                                <div class="taskProgress progressSlim progressRed">32</div>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
									<span class="header">
										<span class="title">SEO for new sites</span>
										<span class="percent"></span>
									</span>
                                <div class="taskProgress progressSlim progressGreen">63</div>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
									<span class="header">
										<span class="title">New blog posts</span>
										<span class="percent"></span>
									</span>
                                <div class="taskProgress progressSlim progressPink">80</div>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-menu-sub-footer">View all tasks</a>
                        </li>
                    </ul>
                </li>
                <!-- end: Notifications Dropdown -->
                <!-- start: Message Dropdown -->
                <li class="dropdown hidden-xs">
                    <a class="btn dropdown-toggle" data-toggle="dropdown" href="form-elements.html#">
                        <i class="fa fa-envelope"></i>
                    </a>
                    <ul class="dropdown-menu messages">
                        <li>
                            <span class="dropdown-menu-title">You have 9 messages</span>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="avatar"><img src="<s:url value="/assets/img/avatar.jpg"/>"
                                                          alt="Avatar"></span>
									<span class="header">
										<span class="from">
									    	Łukasz Holeczek
									     </span>
										<span class="time">
									    	6 min
									    </span>
									</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="avatar"><img src="<s:url value="/assets/img/avatar2.jpg"/>"
                                                          alt="Avatar"></span>
									<span class="header">
										<span class="from">
									    	Megan Abott
									     </span>
										<span class="time">
									    	56 min
									    </span>
									</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="avatar"><img src="<s:url value="/assets/img/avatar3.jpg"/>"
                                                          alt="Avatar"></span>
									<span class="header">
										<span class="from">
									    	Kate Ross
									     </span>
										<span class="time">
									    	3 hours
									    </span>
									</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="avatar"><img src="<s:url value="/assets/img/avatar4.jpg"/>"
                                                          alt="Avatar"></span>
									<span class="header">
										<span class="from">
									    	Julie Blank
									     </span>
										<span class="time">
									    	yesterday
									    </span>
									</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="form-elements.html#">
                                <span class="avatar"><img src="<s:url value="/assets/img/avatar5.jpg"/>"
                                                          alt="Avatar"></span>
									<span class="header">
										<span class="from">
									    	Jane Sanders
									     </span>
										<span class="time">
									    	Jul 25, 2012
									    </span>
									</span>
                                    <span class="message">
                                        Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a class="dropdown-menu-sub-footer">View all messages</a>
                        </li>
                    </ul>
                </li>
                <!-- end: Message Dropdown -->
                <li>
                    <a class="btn" href="form-elements.html#">
                        <i class="fa fa-wrench"></i>
                    </a>
                </li>
                <!-- start: User Dropdown -->
                <li class="dropdown">
                    <a class="btn account dropdown-toggle" data-toggle="dropdown" href="form-elements.html#">
                        <div class="avatar"><img src="<s:url value="/assets/img/avatar.jpg"/>" alt="Avatar"></div>
                        <div class="user">
                            <span class="hello">¡Bienvenido!</span>
                            <span class="name"><s:property value="%{#session.usuario.nombreUsuario}"/></span>
                        </div>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="dropdown-menu-title">

                        </li>
                        <li><a href="form-elements.html#"><i class="fa fa-user"></i> Profile</a></li>
                        <li><a href="form-elements.html#"><i class="fa fa-cog"></i> Settings</a></li>
                        <li><a href="form-elements.html#"><i class="fa fa-envelope"></i> Messages</a></li>
                        <li><a href="<s:url action="CerrarSesion" namespace="/" />"><i class="fa fa-off"></i> Logout</a>
                        </li>
                    </ul>
                </li>
                <!-- end: User Dropdown -->
            </ul>
        </div>
        <!-- end: Header Menu -->

    </div>
</header>
<!-- end: Header -->

</s:if>

<div class="container">
    <div class="row">

        <s:if test="%{#session.usuario != null}">   <!-- start: Main Menu -->
        <div id="sidebar-left" class="col-lg-2 col-sm-1 hidden-print">

            <input type="text" class="search hidden-sm" placeholder="..."/>


            <div class="nav-collapse sidebar-nav collapse navbar-collapse bs-navbar-collapse">
                <ul class="nav nav-tabs nav-stacked main-menu">
                    <li
                            <s:if test="#context['struts.actionMapping'].namespace == '/socio'">class="active"</s:if> >
                        <a href="<s:url action="ListarSocios" namespace="/socio" />"><i
                                class="fa fa-bar-chart-o"></i><span class="hidden-sm"> Socios</span></a></li>
                    <pandero:confirmarperfil perfilEsperado="administrador">
                        <li
                                <s:if test="#context['struts.actionMapping'].namespace == '/usuario'">class="active"</s:if> >
                            <a href="<s:url action="ListarUsuarios" namespace="/usuario"/>"><i
                                    class="fa fa-users"></i><span class="hidden-sm"> Usuarios</span></a></li>
                    </pandero:confirmarperfil>
                    <li>
                        <a class="dropmenu" href="#"><i class="fa fa-list-ol"></i><span
                                class="hidden-sm"> Reportes</span> <span class="label">4</span></a>
                        <ul>
                            <li><a class="submenu"
                                   href="<s:url action="VerSociosYObligaciones_execute" namespace="/reporte"/>"><i
                                    class="fa fa-male"></i><span class="hidden-sm"> Socios y obligaciones</span></a>
                            </li>
                            <li><a class="submenu"
                                   href="<s:url action="VerCuotasPagadasPorSocio_execute" namespace="/reporte"/>"><i
                                    class="fa fa-calendar"></i><span class="hidden-sm"> Cuotas pagadas por socio</span></a>
                            </li>
                            <li><a class="submenu"
                                   href="<s:url action="VerIngresosYEstimadosDiarios_execute" namespace="/reporte"/>"><i
                                    class="fa fa-money"></i><span class="hidden-sm"> Ingresos y estimados diarios</span></a>
                            </li>
                            <li><a class="submenu"
                                   href="<s:url action="VerCuotasVencidas_execute" namespace="/reporte"/>"><i
                                    class="fa fa-usd"></i><span class="hidden-sm"> Cuotas vencidas</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- end: Main Menu -->
        </s:if>
        <s:else>
        <style type="text/css">
            #content {
                background: #383e4b;
            }
        </style>

        </s:else>

        <!-- start: Content -->
        <div id="content" class="<s:if test="%{#session.usuario != null}">col-lg-10 col-sm-11</s:if>">

            <s:if test="actionMessages.size != 0">
            <div class="alert alert-info"><s:actionmessage/></div>
            </s:if>

            <s:if test="actionErrors.size != 0">
            <div class="alert alert-warning"><s:actionerror/></div>
            </s:if>

            <s:if test="fieldErrors.size != 0">
            <div class="alert alert-danger"><s:fielderror/></div>
            </s:if>


