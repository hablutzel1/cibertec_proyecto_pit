<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="./incluidos/cabecera.jsp"%>

<div class="row">
    <div class="login-box">
        <h2>Iniciar sesión</h2>
        <s:form action="IniciarSesion" class="form-horizontal">
            <fieldset>


                <s:textfield name="usuario.nombreUsuario" placeholder="Ingrese usuario" class="input-large col-xs-12" autofocus="autofocus" />

                <s:password class="input-large col-xs-12" name="usuario.contrasena" placeholder="Ingrese contraseña" />

                <div class="clearfix"></div>

                <button type="submit" class="btn btn-primary col-xs-12">Ingresar</button>
            </fieldset>

        </s:form>
    </div>
</div>
<%@ include file="./incluidos/pie.jsp"%>