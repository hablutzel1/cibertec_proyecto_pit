<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp"%>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-edit"></i>Registro de usuario</h2>
                <div class="box-icon">
                    <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content ">

                <s:form action="RegistrarUsuario" >

                    <div class="form-group ">
                        <label class="control-label" for="dni">Nombre de usuario</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="dni" name="usuario.nombreUsuario"/>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="control-label" for="nombres">Contraseña</label>
                        <div class="controls">
                            <s:password class="form-control" id="nombres" name="usuario.contrasena"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Perfil</label>
                        <div class="controls">
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="usuario.perfil"  value="secretaria" checked="">
                                Secretaria
                            </label>
                            <label class="radio">
                                <input type="radio" name="usuario.perfil"  value="administrador" >
                                Administrador
                            </label>

                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a class="btn" href="<s:url action="ListarUsuarios"/>">Cancelar</a>
                    </div>
                </s:form>

            </div>
        </div>
    </div><!--/col-->

</div><!--/row-->

<%@include file="../incluidos/pie.jsp"%>