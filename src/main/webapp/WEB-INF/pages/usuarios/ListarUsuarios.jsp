<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp"%>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-align-justify"></i><span class="break"></span>Usuarios</h2>
                <div class="box-icon">
                    <a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Contraseña</th>
                        <th>Perfil</th>
                    </tr>
                    </thead>
                    <tbody>

                    <s:iterator value="todosLosUsuarios">
                        <tr>
                            <td>${nombreUsuario}</td>
                            <td class="center">********</td>
                            <td class="center">${perfil}</td>
                        </tr>
                    </s:iterator>

                    </tbody>
                </table>
                <div class="pagination pagination-centered">
                    <ul class="pagination">
                        <li><a href="table.html#">Prev</a></li>
                        <li class="active">
                            <a href="table.html#">1</a>
                        </li>
                        <li><a href="table.html#">2</a></li>
                        <li><a href="table.html#">3</a></li>
                        <li><a href="table.html#">4</a></li>
                        <li><a href="table.html#">Next</a></li>
                    </ul>
                </div>
                <p><a href="<s:url action="RegistrarUsuario" />" class="btn btn-large"><i class="fa fa-plus"></i> Añadir</a></p>
            </div>
        </div>
    </div><!--/col-->

<!--/col-->
</div>

<%@include file="../incluidos/pie.jsp"%>