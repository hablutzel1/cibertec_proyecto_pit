<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-edit"></i> Cuotas pagadas por socio</h2>
                <div class="box-icon">
                    <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content ">
                <s:form action="VerCuotasPagadasPorSocio_execute" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label" for="todosLosSocios">Socio</label>
                        <div class="controls">
                            <s:select id="todosLosSocios" list="todosLosSocios" listValue="%{nombres + ' ' + apellidos}"
                                      listKey="codigo" class="form-control" name="socio.codigo"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="fechaDeInicio">Fecha de inicio</label>
                        <div class="controls">
                            <div class="input-group col-sm-4">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="fechaDeInicio" class="form-control" id="fechaDeInicio"
                                       value="<s:date name="fechaDeInicio" format="dd/MM/yyyy"/>"/>
                            </div>
                            <span class="help-block col-sm-8">ej. 03/07/2016</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="fechaDeFin">Fecha de fin</label>
                        <div class="controls">
                            <div class="input-group col-sm-4">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="fechaDeFin" class="form-control" id="fechaDeFin"
                                       value="<s:date name="fechaDeFin" format="dd/MM/yyyy"/>"/>
                            </div>
                            <span class="help-block col-sm-8">ej. 03/07/2016</span>
                        </div>
                    </div>


                    <s:set name="jSPieDePagina">
                        <script type="text/javascript">
                            $("#fechaDeInicio, #fechaDeFin").mask("99/99/9999");
                        </script>
                    </s:set>


                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Generar reporte</button>
                    </div>

                </s:form>
                <br/>
                <s:if test="%{socio != null && fechaDeInicio != null && fechaDeFin != null}">
                    <iframe style="width: 100%;height: 800px;" src="<s:url action="VerCuotasPagadasPorSocio_reporte" >
                    <s:param name="socio.codigo" value="socio.codigo"/>
                    <s:param name="fechaDeInicio">
                        <s:date name="fechaDeInicio" format="dd/MM/yyyy"/>
                    </s:param>
                     <s:param name="fechaDeFin">
                        <s:date name="fechaDeFin" format="dd/MM/yyyy"/>
                    </s:param></s:url>"></iframe>
                </s:if>
            </div>
        </div>
    </div><!--/col-->

</div>
<!--/row-->

<%@include file="../incluidos/pie.jsp" %>