<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-edit"></i> Ingresos y estimados diarios</h2>
                <div class="box-icon">
                    <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content ">
                <s:form action="VerIngresosYEstimadosDiarios_execute" class="form-horizontal">

                    <div class="form-group">
                        <label class="control-label" for="fecha">Fecha</label>
                        <div class="controls">
                            <div class="input-group col-sm-4">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="fecha" class="form-control" id="fecha"
                                       value="<s:date name="fecha" format="dd/MM/yyyy"/>"/>
                            </div>
                            <span class="help-block col-sm-8">ej. 03/07/2016</span>
                        </div>
                    </div>

                    <s:set name="jSPieDePagina">
                        <script type="text/javascript">
                            $("#fecha").mask("99/99/9999");
                        </script>
                    </s:set>


                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Generar reporte</button>
                    </div>

                </s:form>
                <br/>
                <s:if test="%{fecha != null}">
                    <h1>Estimados</h1>
                    <iframe style="width: 100%;height: 600px;"
                            src="<s:url action="VerIngresosYEstimadosDiarios_reporteestimadosdiarios" >
                    <s:param name="fecha">
                        <s:date name="fecha" format="dd/MM/yyyy"/>
                    </s:param>
</s:url>"></iframe>
                    <h1>Ingresos</h1>
                    <iframe style="width: 100%;height: 600px;"
                            src="<s:url action="VerIngresosYEstimadosDiarios_reporteingresosdiarios" >
                    <s:param name="fecha">
                        <s:date name="fecha" format="dd/MM/yyyy"/>
                    </s:param>
</s:url>"></iframe>
                </s:if>
            </div>
        </div>
    </div><!--/col-->

</div>
<!--/row-->

<%@include file="../incluidos/pie.jsp" %>