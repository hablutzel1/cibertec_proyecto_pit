<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-edit"></i>Modificación de obligación de pago</h2>
                <div class="box-icon">
                    <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content ">

                <s:form action="ModificarObligacionDePago" enctype="multipart/form-data">

                    <s:hidden name="obligacionDePago.codigo" value="%{obligacionDePago.codigo}"/>

                    <div class="form-group">
                        <label class="control-label">DNI</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="obligacionDePago.socio.dni"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Nombres</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="obligacionDePago.socio.nombres"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Apellidos</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="obligacionDePago.socio.apellidos"/> </span>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="control-label" for="descripcion">Motivo de pago/deuda</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="descripcion" name="obligacionDePago.descripcion"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Tipo de obligación</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:if test="obligacionDePago.numeroDeCuotas == -1">Sin fecha de vencimiento</s:if><s:else>Regular</s:else> </span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label">Monto <s:if test="obligacionDePago.numeroDeCuotas == -1">cuota</s:if><s:else>total</s:else></label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="obligacionDePago.monto"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Número de cuotas</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="%{obligacionDePago.cuotas.size()}"/> <s:if test="obligacionDePago.numeroDeCuotas == -1"> (Repetitivas)</s:if> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Cuotas pendientes de pago</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property
                                    value="obligacionDePago.numeroDeCuotasPendientesDePago"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Fecha de emisión</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:date name="obligacionDePago.fechaDeEmision"
                                                                                format="dd/MM/yyyy"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Documentos de sustento actuales</label>
                        <div class="controls">
                            <s:if test="obligacionDePago.documentosDeSustento.size > 0">
                                <ul>
                                    <s:iterator value="obligacionDePago.documentosDeSustento" >
                                        <li style="margin-bottom: 5px" ><span class="filetype pdf">${key}</span></li>
                                    </s:iterator>
                                </ul>
                            </s:if>
                            <s:else>-</s:else>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="documentosDeSustentoParaModificacion">Documentos de sustento para modificación</label>
                        <div class="controls">
                            <s:file multiple="multiple" name="documentoDeSustentoParaModificacion" id="documentosDeSustentoParaModificacion"
                                    accept=".pdf"/>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a class="btn"
                           href="<s:url action="ListarObligacionesDePago" ><s:param name="socio.codigo" value="obligacionDePago.socio.codigo"/></s:url>">Cancelar</a>

                    </div>
                </s:form>

            </div>
        </div>
    </div><!--/col-->

</div>
<!--/row-->

<%@include file="../incluidos/pie.jsp" %>