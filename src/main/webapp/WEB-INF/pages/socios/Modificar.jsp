<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp"%>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-edit"></i>Modificar socio</h2>
                <div class="box-icon">
                    <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content ">

                <s:form action="Modificar" >

                    <s:hidden name="socio.codigo"/>

                    <div class="form-group">
                        <label class="control-label">DNI</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.dni"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Nombres</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.nombres"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Apellidos</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.apellidos"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Fecha de nacimiento</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:date name="socio.fechaNacimiento" format="dd/MM/yyyy"/></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Teléfono</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.telefono"/> </span>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="control-label" for="direccion">Dirección</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="direccion" name="socio.direccion"/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label" for="datos_del_conyuge">Datos del cónyuge</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="datos_del_conyuge" name="socio.datosDelConyuge"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Hijos</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.hijos"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Imagen</label>
                        <div class="controls">
                            <img class="img-thumbnail" src="<s:url action="VerDetalle_imagen"><s:param name="socio.codigo" value="socio.codigo"/></s:url>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Huella</label>
                        <div class="controls">
                            <img class="img-thumbnail" src="<s:url action="VerDetalle_huella"><s:param name="socio.codigo" value="socio.codigo"/></s:url>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Firma manuscrita</label>
                        <div class="controls">
                            <img class="img-thumbnail" src="<s:url action="VerDetalle_firma"><s:param name="socio.codigo" value="socio.codigo"/></s:url>"/>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="control-label" for="razon-de-modificacion">Última razón de modificación</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="razon-de-modificacion" name="socio.ultimaRazonDeModificacion"/>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a class="btn" href="<s:url action="ListarSocios"/>">Cancelar</a>
                    </div>
                </s:form>

            </div>
        </div>
    </div><!--/col-->

</div><!--/row-->

<%@include file="../incluidos/pie.jsp"%>