<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header hidden-print">
                <h2><i class="fa fa-align-justify"></i><span class="break"></span>Carnet de socio para <s:property
                        value="%{socio.nombres}"/> <s:property value="socio.apellidos"/></h2>
                <div class="box-icon">
                    <a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="panel panel-default" style="width: 500px">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-8">
                                <h1>Socio Pandero</h1>
                                <ul>
                                    <li><strong>Código:</strong> <fmt:formatNumber pattern="000000"
                                                                                   value="${socio.codigo}"/></li>
                                    <li><strong>DNI:</strong> <s:property value="%{socio.dni}"/></li>
                                    <li><strong>Nombres:</strong> <s:property value="%{socio.nombres}"/> <s:property
                                            value="%{socio.apellidos}"/></li>
                                    <li><strong>Dirección:</strong> <s:property value="%{socio.direccion}"/></li>
                                </ul>
                                <div class="row">
                                    <div class="col-lg-offset-2">

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 text-center">
                                <img style="width: 120px"
                                     src="<s:url action="ImprimirCarnet_codigoqr"><s:param name="socio.codigo" value="socio.codigo"/></s:url>"/>
                                <img src="<s:url action="ImprimirCarnet_code39"><s:param name="socio.codigo" value="socio.codigo"/></s:url>"/>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="form-actions hidden-print">
                    <a class="btn"
                       href="javascript:window.print()">
                        <i class="fa fa-print"></i> Imprimir
                    </a>
                </div>

            </div>
        </div>
    </div><!--/col-->

    <!--/col-->
</div>

<%@include file="../incluidos/pie.jsp" %>