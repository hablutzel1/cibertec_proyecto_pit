<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-align-justify"></i><span class="break"></span>Socios</h2>
                <div class="box-icon">
                    <a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>DNI</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    <s:iterator value="todosLosSocios">
                        <tr>
                            <td>${codigo}</td>
                            <td class="center">${dni}</td>
                            <td class="center">${nombres}</td>
                            <td class="center">${apellidos}</td>
                            <td class="center">
                                <a class="btn"
                                   href="<s:url action="VerDetalle"><s:param name="socio.codigo" value="codigo"/></s:url>">
                                    <i class="fa fa-search-plus "></i> Ver detalles
                                </a>
                                <pandero:confirmarperfil perfilEsperado="administrador">
                                    <a class="btn"
                                       href="<s:url action="Modificar"><s:param name="socio.codigo" value="codigo"/></s:url>">
                                        <i class="fa fa-pencil-square-o"></i> Modificar
                                    </a>
                                </pandero:confirmarperfil>
                                <pandero:confirmarperfil perfilEsperado="secretaria">
                                    <a class="btn"
                                       href="<s:url action="GenerarObligacionDePago"><s:param name="socio.codigo" value="codigo"/></s:url>">
                                        <i class="fa fa-cog"></i> Generar obligación de pago
                                    </a>
                                </pandero:confirmarperfil>
                                <a class="btn"
                                   href="<s:url action="ListarObligacionesDePago"><s:param name="socio.codigo" value="codigo"/></s:url>">
                                    <i class="fa fa-money"></i> Ver obligaciones de pago
                                </a>
                                <a class="btn"
                                   href="<s:url action="ImprimirCarnet"><s:param name="socio.codigo" value="codigo"/></s:url>">
                                    <i class="fa fa-tag"></i> Imprimir carnet de socio
                                </a>
                            </td>
                        </tr>
                    </s:iterator>

                    </tbody>
                </table>
                <div class="pagination pagination-centered">
                    <ul class="pagination">
                        <li><a href="table.html#">Prev</a></li>
                        <li class="active">
                            <a href="table.html#">1</a>
                        </li>
                        <li><a href="table.html#">2</a></li>
                        <li><a href="table.html#">3</a></li>
                        <li><a href="table.html#">4</a></li>
                        <li><a href="table.html#">Next</a></li>
                    </ul>
                </div>
                <p><a href="<s:url action="RegistrarSocio" />" class="btn btn-large"><i class="fa fa-plus"></i>
                    Añadir socio</a></p>
            </div>
        </div>
    </div><!--/col-->

    <!--/col-->
</div>

<%@include file="../incluidos/pie.jsp" %>