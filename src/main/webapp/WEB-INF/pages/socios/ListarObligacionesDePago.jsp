<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-align-justify"></i><span class="break"></span>Obligaciones de pago para <s:property
                        value="%{socio.nombres}"/> <s:property value="socio.apellidos"/></h2>
                <div class="box-icon">
                    <a href="table.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="table.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="table.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Motivo de pago/deuda</th>
                        <th>Tipo de obligación</th>
                        <th>Monto</th>
                        <th>Número de cuotas</th>
                        <th>Cuotas pendientes de pago</th>
                        <th>Fecha de emisión</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    <s:iterator value="socio.obligacionesDePago">
                        <tr>
                            <td>${codigo}</td>
                            <td class="center">${descripcion}</td>
                            <td class="center"><s:if test="numeroDeCuotas == -1">Sin fecha de vencimiento</s:if><s:else>Regular</s:else></td>
                            <td class="center">${monto} <s:if test="numeroDeCuotas == -1"> (por cuota)</s:if><s:else>(total)</s:else></td>
                            <td class="center"><s:property value="%{cuotas.size()}"/></td>
                            <td class="center">${numeroDeCuotasPendientesDePago}</td>
                            <td class="center"><s:date name="fechaDeEmision"
                                                       format="dd/MM/yyyy"/></td>
                            <td class="center">
                                <a class="btn"
                                   href="<s:url action="VerDetalleDeObligacionDePago"><s:param name="obligacionDePago.codigo" value="codigo"/></s:url>">
                                    <i class="fa fa-search-plus "></i> Ver detalle y cuotas
                                </a>
                                <a class="btn"
                                   href="<s:url action="ModificarObligacionDePago"><s:param name="obligacionDePago.codigo" value="codigo"/></s:url>">
                                    <i class="fa fa-edit"></i> Modificar
                                </a>
                            </td>
                        </tr>
                    </s:iterator>

                    </tbody>
                </table>
                <div class="pagination pagination-centered">
                    <ul class="pagination">
                        <li><a href="table.html#">Prev</a></li>
                        <li class="active">
                            <a href="table.html#">1</a>
                        </li>
                        <li><a href="table.html#">2</a></li>
                        <li><a href="table.html#">3</a></li>
                        <li><a href="table.html#">4</a></li>
                        <li><a href="table.html#">Next</a></li>
                    </ul>
                </div>

                <div class="form-actions">
                    <a class="btn" href="<s:url action="ListarSocios"/>">Volver</a>
                    <pandero:confirmarperfil perfilEsperado="secretaria">
                        <a class="btn"
                           href="<s:url action="GenerarObligacionDePago"><s:param name="socio.codigo" value="socio.codigo"/></s:url>">
                            <i class="fa fa-cog"></i> Generar nueva obligación de pago
                        </a>
                    </pandero:confirmarperfil>
                </div>
            </div>
        </div>
    </div><!--/col-->

    <!--/col-->
</div>

<%@include file="../incluidos/pie.jsp" %>