<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp"%>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-edit"></i>Detalles de socio</h2>
                <div class="box-icon">
                    <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content ">

                <div >
                    <div class="form-group">
                        <label class="control-label">DNI</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.dni"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Nombres</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.nombres"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Apellidos</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.apellidos"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Fecha de nacimiento</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:date name="socio.fechaNacimiento" format="dd/MM/yyyy"/></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Teléfono</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.telefono"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Dirección</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.direccion"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Datos del cónyuge</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.datosDelConyuge"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Hijos</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.hijos"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Imagen</label>
                        <div class="controls">
                            <img class="img-thumbnail" src="<s:url action="VerDetalle_imagen"><s:param name="socio.codigo" value="socio.codigo"/></s:url>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Huella</label>
                        <div class="controls">
                            <img class="img-thumbnail" src="<s:url action="VerDetalle_huella"><s:param name="socio.codigo" value="socio.codigo"/></s:url>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Firma manuscrita</label>
                        <div class="controls">
                            <img class="img-thumbnail" src="<s:url action="VerDetalle_firma"><s:param name="socio.codigo" value="socio.codigo"/></s:url>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Última razón de modificación</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="%{socio.ultimaRazonDeModificacion == null ?'-':socio.ultimaRazonDeModificacion}"/></span>
                        </div>
                    </div>

                    <div class="form-actions">
                        <a class="btn" href="<s:url action="ListarSocios"/>">Volver</a>
                    </div>
                </div>

            </div>
        </div>
    </div><!--/col-->

</div><!--/row-->

<%@include file="../incluidos/pie.jsp"%>