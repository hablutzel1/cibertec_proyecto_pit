<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-edit"></i>Generación de obligación de pago</h2>
                <div class="box-icon">
                    <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content ">

                <s:form action="GenerarObligacionDePago" enctype="multipart/form-data">

                    <s:hidden name="socio.codigo"/>

                    <s:hidden name="obligacionDePago.socio.codigo" value="%{socio.codigo}"/>

                    <div class="form-group">
                        <label class="control-label">DNI</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.dni"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Nombres</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.nombres"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Apellidos</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property value="socio.apellidos"/> </span>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="control-label" for="descripcion">Motivo de pago/deuda</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="descripcion" name="obligacionDePago.descripcion"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="controls">
                            <label class="checkbox inline">
                                <s:checkbox id="cuotasRepetitivas" name="obligacionDePago.cuotasRepetitivas"/> Cuotas
                                repetitivas (sin fecha de vencimiento)
                                <s:set name="jSPieDePagina">
                                    <script language="JavaScript">
                                        $(document).ready(function () {
                                            $('#cuotasRepetitivas').change(function () {
                                                if ($(this).is(":checked")) {
                                                    $("#numeroDeCuotas-form-group").hide();
                                                    $("#monto-label").text("Monto por cuota");
                                                } else {
                                                    $("#numeroDeCuotas-form-group").show();
                                                    $("#monto-label").text("Monto total");
                                                }
                                            }).change();
                                        });
                                    </script>
                                </s:set>
                            </label>
                        </div>
                    </div>

                    <div class="form-group" >
                        <label class="control-label" for="monto" id="monto-label"></label>
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <span class="input-group-addon">S/.</span>
                                <s:textfield class="form-control" id="monto" name="obligacionDePago.monto"
                                             type="number" step="0.01"/>
                            </div>
                        </div>
                    </div>


                    <div class="form-group" id="numeroDeCuotas-form-group">
                        <label class="control-label" for="numeroDeCuotas">Número de cuotas</label>
                        <div class="controls">
                            <select id="numeroDeCuotas" class="form-control" name="obligacionDePago.numeroDeCuotas">
                                <option value="1">Cuota única</option>
                                <option>12</option>
                                <option>24</option>
                                <option>36</option>
                            </select>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="control-label" for="documentoDeSustento">Documentos de sustento</label>
                        <div class="controls">
                            <s:file multiple="multiple" name="documentoDeSustento" id="documentoDeSustento" accept=".pdf"/>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a class="btn" href="<s:url action="ListarSocios" namespace="/socio" />">Cancelar</a>
                    </div>
                </s:form>

            </div>
        </div>
    </div><!--/col-->

</div>
<!--/row-->

<%@include file="../incluidos/pie.jsp" %>