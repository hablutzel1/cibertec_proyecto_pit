<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-edit"></i>Detalles de obligación de pago</h2>
                <div class="box-icon">
                    <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content ">

                <s:form action="RegistrarPagoDeCuotas">


                    <div class="form-group">
                        <label class="control-label">DNI</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property
                                    value="obligacionDePago.socio.dni"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Nombres</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property
                                    value="obligacionDePago.socio.nombres"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Apellidos</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property
                                    value="obligacionDePago.socio.apellidos"/> </span>
                        </div>
                    </div>

                    <s:hidden name="obligacionDePago.codigo"/>

                    <div class="form-group">
                        <label class="control-label">Motivo de pago/deuda</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property
                                    value="obligacionDePago.descripcion"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Tipo de obligación</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:if test="obligacionDePago.numeroDeCuotas == -1">Sin fecha de vencimiento</s:if><s:else>Regular</s:else> </span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label">Monto <s:if test="obligacionDePago.numeroDeCuotas == -1">cuota</s:if><s:else>total</s:else></label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property
                                    value="obligacionDePago.monto"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Número de cuotas</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input">
                                <s:property value="%{obligacionDePago.cuotas.size()}"/> <s:if test="obligacionDePago.numeroDeCuotas == -1"> (Repetitivas)</s:if>
                            </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Cuotas pendientes de pago</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:property
                                    value="obligacionDePago.numeroDeCuotasPendientesDePago"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Fecha de emisión</label>
                        <div class="controls">
                            <span class="input-xlarge uneditable-input"><s:date name="obligacionDePago.fechaDeEmision"
                                                                                format="dd/MM/yyyy"/> </span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Documentos de sustento</label>
                        <div class="controls">
                            <s:if test="obligacionDePago.documentosDeSustento.size > 0">
                                <ul>
                                    <s:iterator value="obligacionDePago.documentosDeSustento" >
                                        <li style="margin-bottom: 5px" ><a class="filetype pdf" href="<s:url action="VerDetalleDeObligacionDePago_documento"><s:param name="obligacionDePago.codigo" value="obligacionDePago.codigo"/><s:param name="nombreDeDocumento" value="%{key}" /></s:url>">${key}</a></li>
                                    </s:iterator>
                                </ul>
                            </s:if>
                            <s:else>-</s:else>
                        </div>
                    </div>

                    <table class="table" id="tablaDeCuotas">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Código</th>
                            <th>Monto</th>
                            <th>Fecha de vencimiento</th>
                            <th>Intereses a la fecha (<s:property value="@pe.edu.cibertec.pandero.example.modelo.Cuota@PORCENTAJE_DE_INTERES_DIARIO" />% diario)</th>
                            <th>Fecha de pago</th>
                            <th>Monto pagado (incluye intereses)</th>
                            <th>Comprobante de pago</th>
                        </tr>
                        </thead>
                        <tbody>

                        <s:iterator value="obligacionDePago.cuotas">
                            <tr>
                                <td>
                                    <s:if test="fechaPago == null">
                                        <s:checkbox name="cuotasSeleccionadas" fieldValue="%{codigo}"/>
                                    </s:if>
                                </td>
                                <td>${codigo}</td>
                                <td class="center">${monto}</td>
                                <td class="center"><s:date name="fechaVencimiento" format="dd/MM/yyyy"/></td>
                                <td class="center"><s:if test="interesesALaFecha != -1">${interesesALaFecha}</s:if><s:else>-</s:else></td>
                                <td class="center"><s:if test="fechaPago != null"><s:date name="fechaPago"
                                                                                          format="dd/MM/yyyy"/></s:if><s:else>-</s:else></td>
                                <td class="center"><s:if test="montoPagado != 0">${montoPagado}</s:if><s:else>-</s:else></td>
                                <td class="center"><s:property
                                        value="%{comprobanteDePago != null?comprobanteDePago:'-'}"/></td>
                            </tr>
                        </s:iterator>

                        </tbody>
                    </table>
                    <s:set name="jSPieDePagina">
                        <script language="JavaScript">
                            $(document).ready(function () {
                                $('input[name=cuotasSeleccionadas]').change(function() {
                                    var totalAPagar = 0;
                                    $("#tablaDeCuotas tbody tr > td input[type=checkbox]").each(function(idx, el) {
                                        if ($(el).is(":checked")) {
                                            var montoDeCuota = $(el).parent().parent().find("td:nth-child(3)").text();
                                            totalAPagar += parseFloat(montoDeCuota);
                                            var interesesParaCuota = $(el).parent().parent().find("td:nth-child(5)").text();
                                            if (interesesParaCuota != "-"){
                                                totalAPagar += parseFloat(interesesParaCuota);
                                            }
                                        }
                                    });
                                    $("#montoACobrar").text(totalAPagar.toFixed(2));
                                });
                            })
                        </script>
                    </s:set>

                    <div class="form-group ">
                        <label class="control-label">Monto a cobrar</label>
                        <div class="controls">
                             <span class="input-xlarge uneditable-input">S/.<span id="montoACobrar">0</span></span>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="control-label" for="descripcion">Código de comprobante de pago</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="descripcion" name="codigoDeComprobanteDePago"/>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Registrar pago para cuotas seleccionadas</button>
                        <a class="btn"
                           href="<s:url action="ModificarObligacionDePago"><s:param name="obligacionDePago.codigo" value="obligacionDePago.codigo"/></s:url>">
                            <i class="fa fa-edit"></i> Modificar
                        </a>
                        <a class="btn"
                           href="<s:url action="ListarObligacionesDePago"><s:param name="socio.codigo" value="obligacionDePago.socio.codigo"/></s:url>">Volver</a>
                    </div>


                </s:form>

            </div>
        </div>
    </div><!--/col-->

</div>
<!--/row-->

<%@include file="../incluidos/pie.jsp" %>