<%@ page pageEncoding="UTF-8" %>
<%@include file="../incluidos/cabecera.jsp" %>

<div class="row">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2><i class="fa fa-edit"></i>Registro de socio</h2>
                <div class="box-icon">
                    <a href="form-elements.html#" class="btn-setting"><i class="fa fa-wrench"></i></a>
                    <a href="form-elements.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                    <a href="form-elements.html#" class="btn-close"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content ">

                <s:form action="RegistrarSocio" id="formulario-registro-socio">

                    <div class="form-group ">
                        <label class="control-label" for="dni">DNI</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="dni" name="socio.dni"/>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="control-label" for="nombres">Nombres</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="nombres" name="socio.nombres"/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label" for="apellidos">Apellidos</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="apellidos" name="socio.apellidos"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="fechaNacimiento">Fecha de nacimiento</label>
                        <div class="controls">
                            <div class="input-group date col-sm-4">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control date-picker"
                                       id="fechaNacimiento" data-date-format="dd/mm/yyyy"
                                       name="socio.fechaNacimiento" value="<s:date name="socio.fechaNacimiento" format="dd/MM/yyyy"/>"/>
                            </div>
                        </div>
                    </div>
                    <s:set name="jSPieDePagina">
                        <script language="JavaScript">
                            $(document).ready(function () {
                                $('.date-picker').datepicker();
                            })
                        </script>
                    </s:set>

                    <div class="form-group ">
                        <label class="control-label" for="telefono">Teléfono</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="telefono" name="socio.telefono"/>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="control-label" for="direccion">Dirección</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="direccion" name="socio.direccion"/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label" for="datos_del_conyuge">Datos del cónyuge</label>
                        <div class="controls">
                            <s:textfield class="form-control" id="datos_del_conyuge" name="socio.datosDelConyuge"/>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label" for="hijos">Hijos</label>
                        <div class="controls">
                                <%-- TODO investigar sobre el input[type=number]. --%>
                            <s:textfield type="number" class="form-control" id="hijos" name="socio.hijos" min="0"
                                         step="1"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Imagen</label>
                        <div class="controls">
                            <s:set name="jSPieDePagina">
                                <s:property value="jSPieDePagina" escapeHtml="false"/>
                                <style type="text/css">
                                    /*Copiado de la clase '.img-thumbnail' de Bootstrap. */
                                    #imagen-de-socio video, #imagen-de-socio img {
                                        display: inline-block;
                                        max-width: 100%;
                                        height: auto;
                                        padding: 4px;
                                        line-height: 1.42857143;
                                        background-color: #fff;
                                        border: 1px solid #ddd;
                                        border-radius: 4px;
                                        -webkit-transition: all .2s ease-in-out;
                                        transition: all .2s ease-in-out;
                                    }
                                </style>
                                <script type="text/javascript" src="<s:url value="/assets/js/webcam.js"/>"></script>
                                <script language="JavaScript">
                                    $(document).ready(function () {

                                        Webcam.set({
                                            // TODO establecer el tamaño apropiado de una foto pasaporte.
                                            width: 320,
                                            height: 240,
                                            image_format: 'png'
                                        });
                                        Webcam.attach('#imagen-de-socio');

                                        $("#capturar-imagen").click(function () {
                                            Webcam.snap(function (dataURIDeImagen) {
                                                $("#imagen-de-socio").html('<img src="' + dataURIDeImagen + '"/>');
                                                // TODO confirmar que este método de recorte de la cadena será compatible con el data URI en todas sus formas.
                                                var soloBase64 = dataURIDeImagen.substring(dataURIDeImagen.indexOf(",") + 1);
                                                $("input[name='socio.imagenComoB64']").val(soloBase64);
                                            });
                                        })

                                    })
                                </script>
                            </s:set>
                                <%-- NOTE que actualmente se está forzando el valor de este campo oculto a un valor vacío para que este no se preserve cuando hayan errores de validación, TODO evaluar permitir preservarlo, en este caso después de un error de validación se debería mostrar inmediatamente la imagen anterior. --%>
                            <s:hidden name="socio.imagenComoB64" value=""/>
                            <div id="imagen-de-socio"></div>
                                <%-- TODO evaluar soportar la recaptura de la imagen. --%>
                            <button id="capturar-imagen" type="button" class="btn btn-default btn-xs"
                                    style="margin-top: 10px">Capturar imagen
                            </button>
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="control-label">Huella dactilar</label>
                        <div class="controls">
                            <a href="#" type="button" class="btn btn-default btn-xs"
                               onclick="alert('Se debería abrir la aplicación encargada de captura la huella y guardarla en C:\\_huellas.')">Capturar
                                huella en aplicación externa</a>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Firma manuscrita</label>
                        <div class="controls">
                            <s:set name="jSPieDePagina">
                                <s:property value="jSPieDePagina" escapeHtml="false"/>
                                <script type="text/javascript"
                                        src="<s:url value="/assets/js/jSignature.min.noconflict.js"/>"></script>
                                <script language="JavaScript">
                                    $(document).ready(function () {
                                        $("#firma-de-socio").jSignature();
                                        $("#formulario-registro-socio").submit(function () {
                                            var firmaDeSocioDiv = $("#firma-de-socio");
                                            var data = firmaDeSocioDiv.jSignature("getData", "native");
                                            if (data.length) { // Será vacío si aún no se ingresa nada.
                                                var dataURIDeFirma = firmaDeSocioDiv.jSignature("getData", "default");
                                                var soloBase64 = dataURIDeFirma.substring(dataURIDeFirma.indexOf(",") + 1);
                                                $("input[name='socio.firmaComoB64']").val(soloBase64);
                                            }
                                        })
                                    })
                                </script>
                            </s:set>
                            <s:hidden name="socio.firmaComoB64" value=""/>
                            <div class="row">
                                <div class="col-md-4">
                                    <div id="firma-de-socio"></div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a class="btn" href="<s:url action="ListarSocios"/>">Cancelar</a>
                    </div>
                </s:form>

                <%--</s:form>--%>

            </div>
        </div>
    </div><!--/col-->

</div>
<!--/row-->

<%@include file="../incluidos/pie.jsp" %>